﻿using UnityEngine;
using System.Collections;

namespace Utilities {

	public abstract class Animatable< Type > : MonoBehaviour {

		[SerializeField] public float _speed = 1;

		private Type _origin;
		protected Type _target;
		protected float _interpolate = 1;
		private bool _reached = true;

		public bool HasReached( ) {

			return( _reached );
		}

		public void SetImmediate( Type target ) {

			SetAnimatedProperty( target );
			_interpolate = 1;
		}

		public void SetTarget( Type target, bool forced = true ) {

			if( _reached || forced ) {

				_origin = GetAnimatedProperty( );
				_interpolate = 0;
				_reached = false;
				_target = target;
				OnSetTarget( );
			}
			else Debug.LogWarning( "Attempt of target set but target not yet reached." );
		}

		public void PlayOneFrame( float deltaTime ) {

			if( _interpolate < 1 && ! _reached ) {

				_interpolate += UnityEngine.Time.deltaTime * _speed;

				SetAnimatedProperty( Interpolate( Mathf.Clamp01( _interpolate ), _origin, _target ));

				if( _interpolate >= 1 ) {

					_reached = true;
					OnReached( );
				}
			}
		}

		protected abstract Type GetAnimatedProperty( );

		protected abstract void SetAnimatedProperty( Type property );

		protected virtual Type Interpolate( float interpolate, Type origin, Type target ) {

			return ( Type ) typeof( Type ).GetMethod( "Lerp" ).Invoke( null, new object[ ] { origin, target, interpolate }); 
		}

		protected virtual void OnSetTarget( ) {}

		protected virtual void OnReached( ) {}
	}

}
