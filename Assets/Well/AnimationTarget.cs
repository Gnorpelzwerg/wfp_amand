using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Well {


public class AnimationTarget : MonoBehaviour {
    
    public AnimationClip defaultClip;

    public Animation GetAnimation( ) {

        var anim = GetComponent< Animation >( );
        if( anim == null ) anim = gameObject.AddComponent< Animation >( );
        return anim;
    }
}


}
