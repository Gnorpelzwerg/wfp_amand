﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Well {

    public class Input : MonoBehaviour {
    
        public WellMaster _master;
        public PivotCamera _pivotCamera;
        
        [Header( "User" )]
        public bool _touchControls;
        public float _zoomSensitivity = 1;
        public float _moveSensitivity = 1;
        public float _touchZoomSensitivity = 0.1f;
        public float _touchMoveSensitivity = 0.5f;
    
        Vector3 _lastMousePosition;

        void LateUpdate( ) {
            
            if( _touchControls && ! Application.isEditor ) {
            
                int touchCount = UnityEngine.Input.touchCount;
            
                if( touchCount > 0 ) {
                
                    Touch touch0 = UnityEngine.Input.GetTouch( 0 );
                    
                    if( touchCount == 1 && touch0.phase == TouchPhase.Moved ) {
                    
                        Vector2 touchDelta = touch0.deltaPosition;
                        _pivotCamera._verticalCurrent += - touchDelta.y * _touchMoveSensitivity;
                        _pivotCamera._horizontalCurrent += touchDelta.x * _touchMoveSensitivity;
                    }
                    else if( touchCount == 2 ) {
                    
                        Touch touch1 = UnityEngine.Input.GetTouch( 1 );
                        
                        if( touch0.phase == TouchPhase.Moved || touch1.phase == TouchPhase.Moved ) {
                        
                            float lastDistance = (( touch0.position - touch0.deltaPosition ) - ( touch1.position - touch1.deltaPosition )).magnitude;
                            float currentDistance = ( touch0.position - touch1.position ).magnitude;
                            _pivotCamera._distanceCurrent += - ( currentDistance - lastDistance ) * _touchZoomSensitivity;
                        }
                    }
                }
            }
            
            if( ! _touchControls || Application.isEditor ) {
            
                if( _lastMousePosition != UnityEngine.Input.mousePosition ) {
            
                    _master.ResetInactivityTimer( );
                }
            
                float scrollDelta = UnityEngine.Input.mouseScrollDelta.y;
                Vector3 mousePosition = UnityEngine.Input.mousePosition;
                Vector3 mouseDelta = mousePosition - _lastMousePosition;
                _lastMousePosition = mousePosition;
                
                if( UnityEngine.Input.GetMouseButton( 0 )) {
        
                    _pivotCamera._verticalCurrent += - mouseDelta.y * _moveSensitivity;
                    _pivotCamera._horizontalCurrent += mouseDelta.x * _moveSensitivity;
                }
                
                _pivotCamera._distanceCurrent += - scrollDelta * _zoomSensitivity;
            }
        }
    }
    
    #if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( Input )) ]
    public class InputInspector : SmartEditor.SmartEditor< Input > {

        protected override void OnSmartInspectorGUI( Input target ){
            
            Expose( "_touchControls" );
            
            if( target._touchControls ) {
            
                Expose( "_touchZoomSensitivity" );
                Expose( "_touchMoveSensitivity" );
            }
            else {
            
                Expose( "_zoomSensitivity" );
                Expose( "_moveSensitivity" );
            }
        }
    }
    
    #endif
}