﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Well {

    public class WellMaster : MonoBehaviour {

        public Dropdown _dropdown;
        public PivotCamera _pivotCamera;
        public InfoPanel _infoPanel;
        public Transform _cancelButton;
        public PointOfInterestSphere _poiSpherePrefab;
        public Camera _activeCamera;
        public AudioSource _audioSource;
        public Camera _defaultReferenceCamera;
        [Header( "User" )]
        public Scene _defaultScene;
        public AudioClip _defaultSceneAudio;
        public float _inactivityTime = 300;
        public LayerMask _transparentLayers;
        [Range( 0, 1 )]
        public float _selectionCircleRadiusRelative = 0.025f;
        public string _quitText = "Quit";
        public Scene[ ] _scenes;
        
        Scene _activeScene;
        Camera[ ] _cameras; 
        List< AnimationTarget > _lastAnimations;
        List< PointOfInterestSphere > _poiSpheres;
        float _lastActivity;
        PointOfInterest _activePointOfInterest;

        public Scene GetActiveScene( ) {

            return _activeScene;
        }

        public void ReadDropdown( ) {
        
            if( _dropdown.options[ _dropdown.value ].text == _quitText ) {
                    
                Application.Quit( );
            }
            else {
           
                SetScene( _scenes[ _dropdown.value ]);
            }
            
            ResetInactivityTimer( );
        }
        
        public void ReceiveDropdownButtonPress( ) {
        
            if( null != _activePointOfInterest ) {
            
                DeactivatePointOfInterest( _activePointOfInterest );
            }
            
            _activePointOfInterest = null;
        }
        
        public void ReceivePointOfInterestClick( PointOfInterestSphere source ) {
            
            if( ! HasBlockingUI( )) {
            
                PointOfInterest previous = _activePointOfInterest;
                _activePointOfInterest = source._follow;
                
                if( ReferenceEquals( previous, _activePointOfInterest )) {
                
                    DeactivatePointOfInterest( _activePointOfInterest );
                    _activePointOfInterest = null;
                }
                else {
                
                    if( ! ReferenceEquals( null, previous )) {
                        
                        DeactivatePointOfInterest( previous );
                    }
                    
                    ActivatePointOfInterest( _activePointOfInterest );
                }
            }
            
            ResetInactivityTimer( );
        }
        
        public void ResetInactivityTimer( ) {
        
            _lastActivity = Time.time;
        }
        
        public void ReceiveInfoPanelClose( ) {
        
            if( _activeScene._cameraType == Scene.CameraType.Pivot ) {
            
                if( ! ReferenceEquals( null, _activePointOfInterest )) {
                    
                    DeactivatePointOfInterest( _activePointOfInterest );
                }
                
                _activePointOfInterest = null;
            }
            else {
            
                _cancelButton.gameObject.SetActive( false );
                SetScene( _defaultScene );
            }
        }

        bool HasBlockingUI( ) {
        
            return EventSystem.current.IsPointerOverGameObject( -1 ) || 
                   EventSystem.current.IsPointerOverGameObject( 0 );
        }

        void ActivatePointOfInterest( PointOfInterest target ) {
        
             if( target._hasInformation ) {
             
                _infoPanel._headline.text = target._name;
                _infoPanel._text.text = target._text;
                _infoPanel.gameObject.SetActive( true );
                
                _cancelButton.gameObject.SetActive( true );
                
                _infoPanel._textBackground.anchorMin = new Vector2( _infoPanel._textBackground.anchorMin.x, Mathf.Lerp( 0.89f, 0, target._textBoxSizeRelative ));
             }
             
             if( target._hasAlternativeCullingMask ) {
             
                _activeCamera.cullingMask = target._alternativeCullingMask;
             }
             
             if( target._hasAnimation ) {
             
                target._animatorTarget.SetTrigger( "Activate" );
             }
             
             if( target._hasAlternativePivot ) {
             
                _pivotCamera.SetPivot( target._alternativePivot );
                _cancelButton.gameObject.SetActive( true );
                _activeCamera.cullingMask &= ~( 1 << LayerMask.NameToLayer( "UI" ));
             }
             else if( target._hideOthersWhenActivated ) {
             
                BuildPointsOfInterest( new PointOfInterest[ ] { target } );
             }
             
             GetCorrespondingSphere( target ).Activate( );
        }

        void DeactivatePointOfInterest( PointOfInterest target ) {
        
            _infoPanel.gameObject.SetActive( false );
            _cancelButton.gameObject.SetActive( false );
            _activeCamera.cullingMask = _activeScene._cullingMask;
            
            if( target._hasAlternativePivot ) {
            
                _pivotCamera.SetPivot( _activeScene._pivot );
            }
            
            if( target._hasAnimation ) {
            
                target._animatorTarget.SetTrigger( "Deactivate" );
            }
            
            GetCorrespondingSphere( target ).Deactivate( );
            
            if( target._hideOthersWhenActivated ) {
            
                BuildScenePointsOfInterest( );
            }
        }

        PointOfInterestSphere GetCorrespondingSphere( PointOfInterest pointOfInterest ) {
        
            PointOfInterestSphere target = null;
            
            foreach( PointOfInterestSphere sphere in _poiSpheres ) {
            
                if( sphere._follow == pointOfInterest ) {
                
                    target = sphere;
                }
            }
            
            return target;
        }

        void Start( ) {
        
            foreach( Animation animation in FindObjectsOfType< Animation >( )) {
            
                animation.playAutomatically = false;
                animation.Stop( );
                animation.Rewind( );
            }
        
            foreach( MeshFilter mf in FindObjectsOfType< MeshFilter >( )) {
            
                Collider collider = mf.GetComponent< Collider >( );
                
                int gameObjectLayerMask = 1 << mf.gameObject.layer;
                bool isTransparent = ( _transparentLayers.value & gameObjectLayerMask ) != 0;
                
                if( collider == null && ! isTransparent ) {
                
                    MeshCollider meshCollider = mf.gameObject.AddComponent< MeshCollider >( );
                    meshCollider.sharedMesh = mf.mesh;
                }
            }
        
            ResetInactivityTimer( );
            _cameras = FindObjectsOfType< Camera >( );
            SetScene( _defaultScene );
            BuildDropdown( );
        }
        
        void Update( ) {
            
            if( Time.time > _lastActivity + _inactivityTime && _activeScene != _defaultScene ) {
            
                if( _activeScene._cameraType == Scene.CameraType.Pivot ) {
                
                    SetScene( _defaultScene );
                    BuildDropdown( );
                }
                else {
                
                    _cancelButton.gameObject.SetActive( true );
                }
            }
        }

        public void SetScene( Scene scene ) {
        
            _activeScene = scene;
            
            foreach( Camera cam in _cameras ) {
            
                cam.enabled = false;
                _activeCamera = null;
            }
            
            if( ! ReferenceEquals( _lastAnimations, null ) && _lastAnimations.Count > 0 ) {
            
                foreach( AnimationTarget animTarget in _lastAnimations ) {
                
                    var anim = animTarget.GetAnimation( );
                    anim.Stop( );
                    if( animTarget.defaultClip != null ) animTarget.defaultClip.SampleAnimation( anim.gameObject, 0f );    
                }
            }
            
            if( _activeScene._cameraType == Scene.CameraType.Pivot ) {
            
                _activeCamera = _pivotCamera.GetComponent< Camera >( );
                
                _activeCamera.CopyFrom( _activeScene._overrideCameraSettings ? _activeScene._referenceCamera : _defaultReferenceCamera );
                _activeCamera.enabled = true;
                _activeCamera.cullingMask = _activeScene._cullingMask;
                _activeCamera.targetDisplay = _defaultReferenceCamera.targetDisplay;
                _activeCamera.rect = _defaultReferenceCamera.rect;
                
                _pivotCamera.SetPivot( _activeScene._pivot );
                _pivotCamera.Initialize( );
            }
            else {
            
                _activeScene._animatedCamera.enabled = true;
                _activeScene._animatedCamera.cullingMask = _activeScene._cullingMask;
                _activeCamera = _activeScene._animatedCamera;
            }
            
            if( _activeScene._hasAnimation ) {
                
                _lastAnimations = new List< AnimationTarget >( );
                
                for( int i = 0; i < _activeScene._animationTargets.Length; ++ i ) {
                
                    var animTarget = _activeScene._animationTargets[ i ];
                    var anim = animTarget.GetAnimation( );
                    if( i < _activeScene._animationClips.Length && _activeScene._animationClips[ i ] != null ) {

                        var clip = _activeScene._animationClips[ i ];
                        anim.AddClip( clip, clip.name );
                        anim.clip = clip;
                    }
                    
                    anim.Play( PlayMode.StopAll );    
                    _lastAnimations.Add( animTarget );
                }
            }
            
            if( null != _activePointOfInterest ) {
            
                DeactivatePointOfInterest( _activePointOfInterest );
            }
            
            _activePointOfInterest = null;
            
            BuildScenePointsOfInterest( );
            ResetInactivityTimer( );
            
            if( _activeScene == _defaultScene && _defaultSceneAudio != null ) {
            
                _audioSource.clip = _defaultSceneAudio;
                _audioSource.Play( );
            }
            else {
            
                _audioSource.Stop( );
            }
        }
        
        void BuildDropdown( ) {
        
            _dropdown.ClearOptions( );
            List< string > options = new List< string >( );
            
            foreach( Scene scene in _scenes ) {
                        
                options.Add( scene._displayName );
             }
            
             options.Add( _quitText );             
  
            _dropdown.AddOptions( options );
            int index = new List< Scene >( _scenes ).IndexOf( _defaultScene );
            
            if( index != -1 ) {
            
                _dropdown.value = index;
            }
        }
        
        void BuildScenePointsOfInterest( ) {
        
            BuildPointsOfInterest( _activeScene._pointsOfInterest );
        }
        
        void BuildPointsOfInterest( PointOfInterest[ ] pointsOfInterest ) {
        
            if( ! ReferenceEquals( _poiSpheres, null )) {
            
                if( _poiSpheres.Count > 0 ) {
                
                    foreach( PointOfInterestSphere sphere in _poiSpheres ) {
                    
                        Destroy( sphere.gameObject );
                    }
                }
            }
            
            if( _activeScene._cameraType == Scene.CameraType.Pivot ) {
            
                PointOfInterestSphere[ ] poiSphereArray = new PointOfInterestSphere[ pointsOfInterest.Length ];
            
                for( int i = 0; i < poiSphereArray.Length; ++ i ) {
            
                    PointOfInterestSphere poiSphere = Instantiate( _poiSpherePrefab, transform ).GetComponent< PointOfInterestSphere >( );
                    poiSphere.transform.SetAsFirstSibling( );
                    poiSphere._master = this;
                    PointOfInterest follow = pointsOfInterest[ i ];
                    poiSphere._follow = follow;
                    poiSphere.transform.position = follow.transform.position;
                    poiSphere.transform.localScale *= follow._sphereRadius * 2;
                    poiSphere.GetComponent< SphereCollider >( ).radius = follow._colliderRadius / poiSphere.transform.localScale.x;
                    poiSphereArray[ i ] = poiSphere;
                    poiSphere.GetComponent< MeshRenderer >( ).material.color = follow._sphereColor;
                }
                
                _poiSpheres = new List< PointOfInterestSphere >( poiSphereArray );
            }
            
            else {
            
                _poiSpheres = null;
                if( pointsOfInterest.Length > 0 ) {
                
                    Debug.LogWarning( "Points of interest not allowed in animated camera scene" );
                }
            }
        }
    }

}
