using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Linq;
using UnityEngine.EventSystems;

namespace Amand {


public class ScreenContent : MonoBehaviour {

    static int playbackSpeedIndex = 0;

    public RenderImage renderImage;
    public Transform toggleButtonParent;
    public ToggleButton toggleButtonPrefab;
    public Transform inputButtonParent;
    public ProtocolInputButton inputButtonPrefab;
    public GameObject textView;
    public Transform textParent;
    public Transform imageParent;
    public Text defaultReferenceText;
    public Image defaultReferenceImage;
    public Image fullScreenReferenceImage;
    public ScrollRect textScrollRect;
    public Text title;
    public GameObject titleParent;
    public Dialog dialog;
    public Dialog identification;
    public VideoPlayer videoPlayer;
    public Text playbackSpeedText;
    public GameObject mediaControlsParent;
    public Color defaultToggleColor;
    public Image backgroundImage;
    public GameObject orderListView;
    public Table orderListTable;
    public Table summaryTable;
    public Dialog orderDialog;
    public Structure.Document orderImagePrefab;
    public Structure.Document orderScenePrefab;
    public Transform temporarySceneParent;
    
    public bool initialized = false;
    public Well.WellMaster wellMaster;
    public float[ ] playbackSpeeds = new float[ ]{ 1, 0.5f, 2 };
    public string lastHash = "";

    //called from animation
    public void DisableRenderImage( ) {

        renderImage.Disable( );
    }

    //called from animation
    public void DetachRenderImage( ) {

        renderImage.Detach( );
        ClearInputs( );

        //in case this was a video
        videoPlayer.enabled = false;
    }

    void ClearInputs( ) {

        Master.ClearChildren( inputButtonParent );
        ClearToggles( );
    }

    void ClearToggles( ) {

        Master.ClearChildren( toggleButtonParent );
    }

    //called from animation
    public void Init( ) {

        InitializeTake( );
    }

    //reset everything per take i.e. this is not reset for additional documents
    void InitializeTake( ) {

        ClearInputs( );
        var group = toggleButtonParent.GetComponent< ToggleGroup >( );
        var take = Master.Get( ).GetCurrentTakeData( );
        var toggleColor = take.overrideToggleColor ? take.toggleColor : defaultToggleColor;
        var toggleSize = take.overrideToggleSize ? take.toggleSize : 1f;

        foreach( var toggle in take.GetToggles( )) {

            var button = Instantiate( toggleButtonPrefab );
            button.toggle = toggle;
            button.parent = this;
            button.transform.SetParent( toggleButtonParent );
            button.transform.localScale = Vector3.one * toggleSize;
            button.colorOn = toggleColor;
            
            if( take.hasJump || take.hasConfirmation && take.isConfirmationExclusive ) button.GetComponent< Toggle >( ).group = group;
        }

        foreach( var input in take.GetProtocolInputs( )) {

            var button = Instantiate( inputButtonPrefab );
            button.input = input;
            button.transform.SetParent( inputButtonParent );
        }

        dialog.toggleInfo.color = toggleColor;
        dialog.Init( take );
        orderDialog.Init( take );
        InitializeTakeOrderList( );
    }

    //reset everything here. this is called more often than InitializeTake
    void Initialize( ) {

        initialized = true;
        textView.SetActive( false );
        renderImage.gameObject.SetActive( false );
        imageParent.gameObject.SetActive( false );
        toggleButtonParent.gameObject.SetActive( false );
        inputButtonParent.gameObject.SetActive( true );
        titleParent.SetActive( false );
        videoPlayer.enabled = false;
        videoPlayer.clip = null;
        videoPlayer.isLooping = true;
        mediaControlsParent.SetActive( false );
        identification.gameObject.SetActive( false );
        backgroundImage.gameObject.SetActive( false );
        orderListView.SetActive( false );
        temporarySceneParent.gameObject.SetActive( false );
        Master.ClearChildren( temporarySceneParent );
    }

    void Start( ) {

        if( ! initialized ) Initialize( );
    }

    public void SetScene( Well.Scene scene ) {

        Initialize( );

        renderImage.gameObject.SetActive( true );
        toggleButtonParent.gameObject.SetActive( true );
        SoftwareLayerSwitch.MaskEverything( );
        var layerSwitch = scene.GetComponent< SoftwareLayerSwitch >( );
        if( layerSwitch ) layerSwitch.Mask( );
        
        //order important here
        wellMaster.SetScene( scene );
        renderImage.Attach( wellMaster._activeCamera );

        SetHasControllableSpeed( scene._hasAnimation && ! Master.Get( ).GetCurrentTakeData( ).hideMediaControls );
    }

    public void SetDocument( Structure.Document document ) {

        Initialize( );

        if( document.type == Structure.DocumentType.text || document.type == Structure.DocumentType.tableOfContents ) {

            textView.SetActive( true );
            var referenceText = document.hasReferenceText ? document.referenceText : defaultReferenceText;
            Master.ClearChildren( textParent );
            var text = Instantiate( referenceText, textParent );
            text.text = document.text;
            textScrollRect.content = text.GetComponent< RectTransform >( );
            textScrollRect.normalizedPosition = new Vector2( 0, 1 );
        }
        else if( document.type == Structure.DocumentType.image ) {

            imageParent.gameObject.SetActive( true );

            var referenceImage = defaultReferenceImage;
            if( document.isFullScreenImage ) referenceImage = fullScreenReferenceImage;
            if( document.hasReferenceImage ) referenceImage = document.referenceImage;

            Master.ClearChildren( imageParent );
            var image = Instantiate( referenceImage, imageParent );
            image.sprite = document.sprite;
        }
        else if( document.type == Structure.DocumentType.audio ) {

            //audio is actually played by master
            var take = Master.Get( ).GetCurrentTakeData( );
            if( take.type == Structure.TakeType.video ) SetVideo( take.video );
        }
        else if( document.type == Structure.DocumentType.scene ) {

            SetScene( document.scene );
        }
        else if( document.type == Structure.DocumentType.video ) {

            SetVideo( document.video );
        }

        if( document.hasName ) {

            SetTitle( document.displayName );
        }
    }

    public void SetTitle( string text ) {

        titleParent.SetActive( true );
        title.text = text;
    }

    public void SetVideo( VideoClip clip ) {

        Initialize( );
        
        var take = Master.Get( ).GetCurrentTakeData( );

        videoPlayer.enabled = true;
        videoPlayer.clip = clip;
        videoPlayer.isLooping = ! take.haltAfterPlayback;
        videoPlayer.Stop( );
        videoPlayer.Play( );

        renderImage.gameObject.SetActive( true );
        renderImage.AttachVideo( videoPlayer );

        SetHasControllableSpeed( ! take.hideMediaControls );
    }

    public void SetHasControllableSpeed( bool hasControllableSpeed ) {

        mediaControlsParent.SetActive( hasControllableSpeed );
        InitializeWithPlaybackSpeedIndex( hasControllableSpeed ? playbackSpeedIndex : 0 );
    }

    public void SetPhoto( ) {

        Initialize( );

        renderImage.gameObject.SetActive( true );
        renderImage.AttachPhoto( );
    } 

    public void SetIdentification( ) {

        Initialize( );
        var take = Master.Get( ).GetCurrentTakeData( );
        identification.Init( take );
        if( take.hasBackground ) {

            backgroundImage.sprite = take.background;
            backgroundImage.gameObject.SetActive( true );
        }
    }

    void InitializeTakeOrderList( ) {

        var take = Master.Get( ).GetCurrentTakeData( );
        var items = take.GetOrderItems( );

        orderListTable.Clear( );
        var columns = new string[ ]{ "piece", "", "designation", "dimensions", "material number", "" };
        var header = orderListTable.AddRow( );
        header.isReadonly = true;
        header.GetComponent< HorizontalLayoutGroup >( ).padding = new RectOffset( 0, 0, 10, 10 );

        foreach( var col in columns ) {

            var data = header.AddData( );
            var text = data.AddText( );
            text.text = col;
            text.fontStyle = FontStyle.Bold;
        }

        var widths = new List< float > { 1, 0.5f, 4, 3, 2, 0.5f };
        var normalizedWidths = widths.Select( x => x / widths.Sum( )); //normalize
        orderListTable.columnWidths = normalizedWidths.ToArray( );

        for( int i = 0; i < items.Length; ++ i ) {

            var item = items[ i ];
            var sprite = item.hasImage ? item.image : null;
            var scene = item.hasScene ? item.scene : null;
            AddOrderListRow( "" + item.piece, sprite, scene, item.designation, item.dimensions, item.materialNumber, false );
        }

        RecomputeSummary( );
    }

    public void SetOrderList( ) {

        Initialize( );
        orderListView.SetActive( true );
    }

    public void SetEmpty( ) {

        Initialize( );
    }

    public void OnCyclePlaybackSpeed( ) {

        playbackSpeedIndex = ( playbackSpeedIndex + 1 ) % playbackSpeeds.Length;
        InitializeWithPlaybackSpeedIndex( playbackSpeedIndex );
    }

    void InitializeWithPlaybackSpeedIndex( int index ) {

        
        var speedNames = new string[ ]{ "1×", "½×", "2×" };

        var speed = playbackSpeeds[ index ];
        videoPlayer.playbackSpeed = speed;
        
        foreach( Animation animation in FindObjectsOfType< Animation >( )) {

            foreach( AnimationState state in animation ) {

                state.speed = speed;
            }
        }

        playbackSpeedText.text = speedNames[ index ];
    }

    public void OnReplay( ) {

        foreach( Animation animation in FindObjectsOfType< Animation >( )) {

            foreach( AnimationState state in animation ) {

                state.time = 0;
            }

            animation.Play( );
        }

        if( videoPlayer.enabled ) {

            videoPlayer.Stop( );
            videoPlayer.Play( );    
        }
    }

    public void OnOrderItemImageClicked( Sprite sprite, Well.Scene scene, string designation, string dimensions ) {

        Structure.Document document = null;

        if( scene == null ) {

            document = Instantiate( orderImagePrefab );
            document.sprite = sprite;
        }
        else {

            document = Instantiate( orderScenePrefab );
            document.scene = scene;
        }

        document.hasName = true;
        document.displayName = $"{ designation } { dimensions }";
        Master.Get( ).OnDocument( document );
        Destroy( document.gameObject );
    }

    public TableRow AddOrderListRow( string piece, Sprite sprite, Well.Scene scene, string designation, string dimensions, string materialNumber, bool isOn ) {

        var row = orderListTable.AddRow( );

        var data = row.AddData( );
        var input = data.AddTableInput( ).inputField;
        input.contentType = InputField.ContentType.IntegerNumber;
        input.text = "" + piece;

        data = row.AddData( );
        var image = data.AddImage( );
        image.sprite = sprite;
        if( sprite == null ) image.color = Color.clear;
        else {

            var trigger = image.gameObject.AddComponent< EventTrigger >( );
            var entry = new EventTrigger.Entry( );
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback.AddListener( _ => OnOrderItemImageClicked( sprite, scene, designation, dimensions ));
            trigger.triggers.Add( entry );
        }

        data = row.AddData( );
        input = data.AddTableInput( ).inputField;
        input.text = designation;
        input.lineType = InputField.LineType.MultiLineNewline;

        data = row.AddData( );
        input = data.AddTableInput( ).inputField;
        input.text = dimensions;

        data = row.AddData( );
        input = data.AddTableInput( ).inputField;
        input.text = materialNumber;

        data = row.AddData( );
        var toggle = data.AddToggle( );
        toggle.isOn = isOn;

        SetSelectedRow( row );
        return row;
    }

    public TableRow AddSummaryRow( int index, string piece, string designation, string dimensions, string materialNumber ) {

        var row = summaryTable.AddRow( );
        row.isReadonly = true;

        var data = row.AddData( );
        var text = data.AddText( );
        text.text = "" + index;
        text.color = new Color( text.color.r, text.color.g, text.color.b, 0.5f );

        data = row.AddData( );
        text = data.AddText( );
        text.text = "" + piece;

        data = row.AddData( );
        text = data.AddText( );
        text.text = designation;

        data = row.AddData( );
        text = data.AddText( );
        text.text = dimensions;

        data = row.AddData( );
        text = data.AddText( );
        text.text = materialNumber;

        if( materialNumber.Trim( ) == "" ) {

            data.normal = new Color( 0.5f, 0, 0 );
            text.text = "required";
        }

        return row;
    }

    public void OnAddOrderListRow( ) {

        AddOrderListRow( "1", null, null, "", "", "", true );
    }

    public void OnDeleteOrderListRow( ) {

        if( orderListTable.selectedRow != null ) orderListTable.RemoveRow( orderListTable.selectedRow );
    }

    public void SetSelectedRow( TableRow target ) {

        orderListTable.selectedRow = target;
    }

    void CheckRecomputeSummary( ) {

        string hash = "";

        for( int i = 1; i < orderListTable.rows.Count; ++ i ) {

            var row = orderListTable.rows[ i ];

            hash = Master.Hash( hash + row.dataElements[ 5 ].toggle.isOn );

            var piece = row.dataElements[ 0 ].input.inputField.text;
            var designation = row.dataElements[ 2 ].input.inputField.text;
            var dimensions = row.dataElements[ 3 ].input.inputField.text;
            var materialNumber = row.dataElements[ 4 ].input.inputField.text;

            hash = Master.Hash( hash + piece );
            hash = Master.Hash( hash + designation );
            hash = Master.Hash( hash + dimensions );
            hash = Master.Hash( hash + materialNumber );
        }

        if( lastHash != hash ) {

            RecomputeSummary( );
            lastHash = hash;
        }
    }

    public void RecomputeSummary( ) {

        summaryTable.Clear( );

        var columns = new string[ ]{ "", "piece", "designation", "dimensions", "material number" };
        var header = summaryTable.AddRow( );
        header.isReadonly = true;
        header.GetComponent< HorizontalLayoutGroup >( ).padding = new RectOffset( 0, 0, 10, 10 );

        foreach( var col in columns ) {

            var data = header.AddData( );
            var text = data.AddText( );
            text.text = col;
            text.fontStyle = FontStyle.Bold;
        }

        var widths = new List< float > { 0.3f, 1, 4, 3, 2 };
        var normalizedWidths = widths.Select( x => x / widths.Sum( )); //normalize
        summaryTable.columnWidths = normalizedWidths.ToArray( );

        int counter = 1;
        for( int i = 1; i < orderListTable.rows.Count; ++ i ) {

            var row = orderListTable.rows[ i ];
            if( row.dataElements[ 5 ].toggle.isOn ) {

                var piece = row.dataElements[ 0 ].input.inputField.text;
                var designation = row.dataElements[ 2 ].input.inputField.text;
                var dimensions = row.dataElements[ 3 ].input.inputField.text;
                var materialNumber = row.dataElements[ 4 ].input.inputField.text;
                AddSummaryRow( counter ++, "" + piece, designation, dimensions, materialNumber );
            }
        }

        Canvas.ForceUpdateCanvases( );
    }

    void Update( ) {

        CheckRecomputeSummary( );
    }
}


}