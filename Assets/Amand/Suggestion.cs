using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class Suggestion : MonoBehaviour {
    
    public Text label;

    public SuggestiveInput parent;

    public void OnClick( ) {

        parent.OnClick( this );
    }
}


}