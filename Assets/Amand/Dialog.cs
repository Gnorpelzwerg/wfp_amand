using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using SimpleJSON;

namespace Amand {

public class Dialog : MonoBehaviour {
 
    public Text title;
    public string titleText;
    public string titleTextSaved;
    public string titleTextSent;
    public string titleTextJump;
    public Text description;
    public Text toggleInfo;
    public GameObject okButton;
    public GameObject status;
    public Text checkMark;
    public Color savedColor;
    public Color sentColor;
    public ScreenContent screenContent;
    public bool isIdentificationDialog; //reduce identification to confirmation
    public bool isOrderDialog; //reduce order to confirmation
    public ProtocolInputButton[ ] inputs;
    public Vector2 defaultPosition;
    public BoundsPosition boundsPosition;
    public TextAsset orderMailStyleAsset;

    public bool isConfirmDialog; 

    public void Init( Structure.Take take ) {

        gameObject.SetActive( false );
        isConfirmDialog = false;

        var isIdentificationTake = take.type == Structure.TakeType.identification;
        var isOrderTake = take.type == Structure.TakeType.orderList;

	if( ! IsSpecialDialog( ) && ( take.hasConfirmation || take.hasJump ) ||
              isIdentificationDialog && isIdentificationTake ||
              isOrderDialog && isOrderTake ) {

			gameObject.SetActive( true );
    		description.text = take.message;
    		toggleInfo.gameObject.SetActive( take.GetToggles( ).Length > 0 );

            if( take.hasConfirmation || IsSpecialDialog( )) {

                isConfirmDialog = true;
            }

            if( ! IsSpecialDialog( )) {

                boundsPosition.positionInBounds = take.dialogPosition;
            }

            if( IsSpecialDialog( )) {

                foreach( var input in inputs ) input.inputField.text = "";
            }
    	}
    }

    bool IsSpecialDialog( ) {

        return isIdentificationDialog || isOrderDialog;
    }

    void Update( ) {

    	if( isConfirmDialog ) {

            if( screenContent == Master.Get( ).activeScreenContent ) {

                var isSaved = Master.Get( ).IsSaved( );
                var isSent = Master.Get( ).IsSent( );

                if( isSent ) {

                    title.text = titleTextSent;
                    checkMark.color = sentColor;
                }
                else if( isSaved ) {

                    title.text = titleTextSaved;
                    checkMark.color = savedColor;
                }
                else {

                    title.text = titleText;
                }

                //only update if not saved
                if( ! isSaved ) toggleInfo.text = GetToggleInfo( );
                okButton.SetActive( ! isSaved && ! isSent );
                status.SetActive( isSaved || isSent );

                if( IsSpecialDialog( )) {

                    foreach( var input in inputs ) input.inputField.interactable = ! isSaved && ! isSent;
                }

                if( screenContent.toggleButtonParent.gameObject.activeSelf && ( isSaved || isSent )) screenContent.toggleButtonParent.gameObject.SetActive( false );
            }
    	}
        else { //this is a jump dialog

            toggleInfo.text = GetToggleInfo( );
            title.text = titleTextJump;
            okButton.SetActive( true );
            status.SetActive( false );
        }

    }

    string GetToggleInfo( ) {

        var identifiers = new List< string >( );

	for( int i = 0; i < screenContent.toggleButtonParent.childCount; ++ i ) {

		var toggleButton = screenContent.toggleButtonParent.GetChild( i ).GetComponent< ToggleButton >( );
    		if( toggleButton.isOn( )) {

                identifiers.Add( toggleButton.toggle.identifier );
    		}
    	}

        identifiers.Sort( );

        var info = "";
        foreach( var identifier in identifiers ) {

            info += " - " + identifier + "\n";
        }

    	return info.TrimEnd( );
    }

    JSONObject GetInputInfo( ) {

        var info = new JSONObject( );

        for( int i = 0; i < screenContent.inputButtonParent.childCount; ++ i ) {

            var inputButton = screenContent.inputButtonParent.GetChild( i ).GetComponent< ProtocolInputButton >( );
            info[ inputButton.input.identifier ] = inputButton.inputField.text;
        }

        foreach( var inputButton in inputs ) {

            info[ inputButton.input.identifier ] = inputButton.inputField.text;
        }


        return info;
    }

    public void OnClick( ) {

    	if( isConfirmDialog ) {

            if( IsSpecialDialog( )) {

                var description = isIdentificationDialog ? "identified" : "order placed";

                foreach( var inputButton in inputs ) {

                    var key = inputButton.input.identifier;
                    var ids = new List< string >( Master.Get( ).GetKnownValues( key ));
                    ids.Add( inputButton.inputField.text.Trim( ));
                    Master.Get( ).SetKnownValues( key, ids.Distinct( ).ToArray( ));
                }

                var info = GetInputInfo( );

                if( isOrderDialog ) {

                    screenContent.RecomputeSummary( );
                    var table = screenContent.summaryTable;
                    var csv = "";

                    var html = @"<head>
                                    <link href=""https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap"" rel=""stylesheet"">
                                </head>
                                <body>";

                    var subject = $"VEM order #{ Master.Get( ).sessionUid.Substring( 0, 8 )}";
                    html += $"<h2>{ subject }</h2><table>";

                    foreach( var row in table.rows ) {

                        csv += System.String.Join( ",", row.dataElements.Select( data => data.GetData( ))) + "\n";

                        html += "<tr>";
                        foreach( var data in row.dataElements ) html += $"<td>{ data.GetData( )}</td>";
                        html += "</tr>";
                    }

                    html += "</table>";
                    html += "<p>";

                    foreach( var pair in info ) {

                        html += $"<b>{ pair.Key }:</b> {( System.String ) pair.Value }<br>";
                    }

                    html += "</p>";
                    html += $"<style>{ orderMailStyleAsset.text }</style>";
                    html += "</body>";

                    var mailto = "a@b.c";
                    foreach( var button in inputs ) if( button.input.identifier == "order email" ) mailto = button.inputField.text;

                    //send user copy and order mail
                    Master.Get( ).CommitEmail( mailto, $"copy of { subject }", html );
                    Master.Get( ).CommitEmail( "serviceapp@vem-group.com", subject, html );

                    info[ "order csv" ] = csv;
                }

                Master.Get( ).OnConfirm( description, "", info );
            }
            else {

                Master.Get( ).OnConfirm( description.text, GetToggleInfo( ), GetInputInfo( ));
            }
        }
        else {

            Master.Get( ).OnJump( );
        }
    }
}


}
