using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class ProtocolInputButton : MonoBehaviour {

    public BoundsPosition boundsPosition;
    public InputField inputField;
    public LayoutElement inputLayout;
    public RectTransform child;
    public Text description;
    public Text placeholder;

    public int defaultPadding = 10;
    public string defaultPlaceholder = "#";
    public Color defaultFieldColor = Color.grey;
    public bool controlLayout = true;

    public Structure.ProtocolInput input;

    float CalculateWidth( int width ) {

        var fullText = "";
        for( int i = 0; i < width; ++ i ) fullText += "_";

        Vector2 extents = inputField.textComponent.rectTransform.rect.size;
        var settings = inputField.textComponent.GetGenerationSettings( extents );
        settings.generateOutOfBounds = true;
        return new TextGenerator( ).GetPreferredWidth( fullText, settings );
    }

    void JumpToNextInput( ) {

        var index = transform.GetSiblingIndex( );

        if( transform.parent.childCount > index + 1 ) {

            var next = transform.parent.GetChild( index + 1 ).GetComponent< ProtocolInputButton >( );
            next.inputField.Select( );
        }
    }

    public void OnInputChanged( ) {

        if( inputField.text.Length == input.width && input.autoJumpOnLimit ) JumpToNextInput( );
    }

    void Update( ) {

        description.gameObject.SetActive( input.description.Length > 0 );
        description.text = input.description;

        if( controlLayout ) {

            boundsPosition.positionInBounds = input.position;
            child.anchoredPosition = input.pixelOffset;
        }

        inputLayout.preferredWidth = CalculateWidth( input.width ) + ( input.overridePadding ? input.padding : defaultPadding );
        placeholder.text = input.overridePlaceholder ? input.placeholder : defaultPlaceholder;
        inputField.characterLimit = input.width;

        if(( Input.GetKeyDown( KeyCode.Return ) || Input.GetKeyDown( KeyCode.Tab )) && inputField.isFocused ) {

            JumpToNextInput( );
        }

        inputField.GetComponent< Image >( ).color = input.overrideColor ? input.fieldColor : defaultFieldColor;
    }
}

}