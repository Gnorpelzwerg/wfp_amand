using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {

public class Table : MonoBehaviour {

    public TableRow rowPrefab;
    public List< TableRow > rows;
    public float targetWidth = 0;
    public float[ ] columnWidths;
    public TableRow selectedRow;

    public void Clear( ) {

        while( transform.childCount > 0 ) {

            var child = transform.GetChild( 0 );
            child.SetParent( null );
            Destroy( child.gameObject );
        }

        foreach( var row in rows ) row.Clear( );
        rows.Clear( );
    }

    public TableRow AddRow( ) {

        var child = Instantiate( rowPrefab );
        child.transform.SetParent( transform );
        rows.Add( child );
        return child;
    }

    public void RemoveRow( TableRow row ) {

        RemoveRowAtIndex( rows.IndexOf( row ));
    }

    public void RemoveRowAtIndex( int index ) {

        if( rows[ index ].isReadonly ) return;

        rows.RemoveAt( index );
        var child = transform.GetChild( index );
        child.SetParent( null );
        Destroy( child.gameObject );
    }

    void Update( ) {

        UpdateWidth( );
        UpdateHeight( );
        foreach( var row in rows ) row.isSelected = row == selectedRow;
    }

    public float CalculateHeight( ) {

        var layout = GetComponent< VerticalLayoutGroup >( );
        float sum = 0;
        foreach( var row in rows ) sum += row.CalculateHeight( );
        sum += layout.padding.top + layout.padding.bottom + Mathf.Max( 0, rows.Count - 1 ) * layout.spacing;
        return sum;
    }

    public void UpdateWidth( ) {

        var targetWidth = ( transform.parent as RectTransform ).rect.width;
        var layout = GetComponent< VerticalLayoutGroup >( );
        var remainingWidth = targetWidth - layout.padding.left - layout.padding.right;
        foreach( var row in rows ) row.SetColumnWidths( remainingWidth, columnWidths );
        //( transform as RectTransform ).SetSizeWithCurrentAnchors( RectTransform.Axis.Horizontal, targetWidth );
    }

    public void UpdateHeight( ) {

        foreach( var row in rows ) row.UpdateHeight( );
        ( transform as RectTransform ).SetSizeWithCurrentAnchors( RectTransform.Axis.Vertical, CalculateHeight( ));
    }
}


}
