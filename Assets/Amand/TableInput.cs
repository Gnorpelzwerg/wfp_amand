using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class TableInput : MonoBehaviour {

    public InputField inputField;
    public TableData parent;

    public float CalculateHeight( ) {

        var fullText = inputField.text;

        Vector2 extents = inputField.textComponent.rectTransform.rect.size;
        var settings = inputField.textComponent.GetGenerationSettings( extents );
        settings.generateOutOfBounds = false;
        return new TextGenerator( ).GetPreferredHeight( fullText, settings );
    }

    public void UpdateHeight( ) {

        ( transform as RectTransform ).SetSizeWithCurrentAnchors( RectTransform.Axis.Vertical, CalculateHeight( ));
    }

    void Update( ) {

        UpdateHeight( );
    }

    public void OnBeginEdit( ) {

        parent.OnBeginEdit( );
    }

    public void OnEndEdit( ) {

        parent.OnEndEdit( );
    }
}


}