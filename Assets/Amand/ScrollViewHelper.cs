using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

namespace Amand {


public class ScrollViewHelper : MonoBehaviour {

    public Table table;
    public float sensitivity = 10;
    public ScrollRect scrollRect;
    public Image scrollbarImage;

    public bool pressedSinceDown = false;
    public float beginMouse;
    public Vector2 beginNormalizedPosition;
    public float lastContentHeight;

    void Update( ) {

        var currentMouse = Input.mousePosition.y;
        float viewHeight = ( scrollRect.transform as RectTransform ).rect.height;
        float contentHeight = ( scrollRect.content.transform as RectTransform ).rect.height;
        float scrollHeight = contentHeight - viewHeight;

        scrollbarImage.color = new Color( scrollbarImage.color.r, scrollbarImage.color.g, scrollbarImage.color.b, lastContentHeight == contentHeight ? 0.1f : 0 );

        //if( lastContentHeight != contentHeight ) Debug.Log( "last: " + lastContentHeight + ", now: " + contentHeight );

        lastContentHeight = contentHeight;

        if( Input.GetMouseButtonDown( 0 ) && IsMouseOverTable( )) {

            beginMouse = currentMouse;
            beginNormalizedPosition = scrollRect.normalizedPosition;
            pressedSinceDown = true;
        }

        pressedSinceDown = pressedSinceDown && Input.GetMouseButton( 0 );

        if( pressedSinceDown ) {

            float mouseDelta = currentMouse - beginMouse;
            scrollRect.normalizedPosition = beginNormalizedPosition + new Vector2( 0, 1 ) * - mouseDelta / Mathf.Max( 1f, scrollHeight );
        }
        else {

            var delta = Input.mouseScrollDelta.y * sensitivity;
            scrollRect.normalizedPosition += new Vector2( 0, 1 ) * delta / Mathf.Max( 1f, scrollHeight );
        }

        scrollRect.normalizedPosition = new Vector2( 0, Mathf.Clamp01( scrollRect.normalizedPosition.y ));
    }

    private bool IsMouseOverTable( ) {

        // Check if the EventSystem and PointerEventData exist
        if (EventSystem.current == null )
            return false;

        // Create a PointerEventData to store the current mouse position
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;

        // Create a list to store the raycast results
        List<RaycastResult> results = new List<RaycastResult>();

        // Perform the raycast to check for UI elements at the mouse position
        EventSystem.current.RaycastAll(eventData, results);

        // Check if any UI element was hit by the raycast
        return results.Select( x => x.gameObject ).Contains( table.gameObject );
    }
}


}