using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SmartEditor;
using System.Linq;

namespace Amand.Structure {

public class Performance : MonoBehaviour {
    
    [TextArea(1, 10)]
    public string displayName;
    public ButtonSettings buttonSettings;
    public bool hasTableOfContents;

    public List< Take > GetTakes( ) {

        return GetComponentsInChildren< Take >( ).OrderBy( c => c.transform.GetSiblingIndex( )).ToList( );
    }

    Take[ ] GetChapterTakes( ) {

        return GetTakes( ).Where( take => take.isChapter ).ToArray( );
    }

    public string GetTableOfContents( ) {

        var label = "";

        foreach( var take in GetChapterTakes( )) {

            var index = $"<b>({ take.transform.GetSiblingIndex( ) + 1 })</b>".PadLeft( 14 );
            label += $"{ index } | { take.chapterDescription }\n";
        }

        return label;
    }
}

#if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( Performance ))]
    public class PerformanceInspector : SmartEditor< Performance > {

        protected override void OnSmartInspectorGUI( Performance target ) {

            Expose( "displayName" );
            Expose( "buttonSettings" );
            Expose( "hasTableOfContents" );

            if( target.hasTableOfContents ) {

                Note( target.GetTableOfContents( ));
            }
        }
    }
    
#endif


}