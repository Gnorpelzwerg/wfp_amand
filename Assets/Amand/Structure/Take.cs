using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using SmartEditor;
using System.Linq;
using UnityEngine.Video;

namespace Amand.Structure {


public enum TakeType { document, scene, reference, video, photo, identification, orderList };

public class Take : SelfReferenceable< Take > {
    
    public TakeType type;
    public Well.Scene scene;
    public Document document;
    public VideoClip video;
    public bool isImportant;
    public bool hasConfirmation;
    public bool isConfirmationExclusive;
    public bool hasOverlayText;
    [TextArea(1,100)]
    public string overlayText;
    public bool hasJump;
    public bool overrideNext;
    public Take next;
    public bool hideMediaControls;
    public bool haltAfterPlayback;
    [TextArea(1,100)]
    public string message;
    public Vector2 dialogPosition = new Vector2( 1, 0 );
    public Vector2 overlayTextPosition = new Vector2( 0.5f, 0 );
    public bool overrideToggleColor;
    public Color toggleColor = new Color( 0, 0.7f, 0.1f, 1 );
    public bool overrideToggleSize;
    public float toggleSize = 1f;
    public bool hasBackground;
    public Sprite background;
    public bool isChapter;
    [TextArea(1,100)]
    public string chapterDescription;
    public bool hasForwardTake;
    public Take forwardTake;
    public bool hasRewindTake;
    public Take rewindTake;

    public JumpToggle[ ] GetJumpToggles( ) {

        return GetComponentsInChildren< JumpToggle >( );
    }

    public ProtocolToggle[ ] GetProtocolToggles( ) {

        return GetComponentsInChildren< ProtocolToggle >( );
    }

    public ProtocolInput[ ] GetProtocolInputs( ) {

        return GetComponentsInChildren< ProtocolInput >( );
    }

    public Toggle[ ] GetToggles( ) {

        return GetComponentsInChildren< Toggle >( );
    }

    public OrderItem[ ] GetOrderItems( ) {

        return GetComponentsInChildren< OrderItem >( );
    }

    public Document[ ] GetDocuments( ) {

        //extension method
        return this.GetReferencedComponentsInChildren< Document >( ).Where( d => type != TakeType.document || d != document ).ToArray( );
    }

    public override bool IsReference( ) { 

        return type == TakeType.reference; 
    }
}

#if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( Take ))]
    public class TakeInspector : SelfReferenceableInspector< Take > {

        void ShowChildrenList( string initialText, Component[ ] children ) {

            var info = initialText + ":\n";

            foreach( var c in children ) {

                info += " - " + c.gameObject.name + "\n";
            }

            Note( children.Length > 0 ? info.Trim( ) : "No " + initialText );
        }

        protected override void OnSelfReferenceableInspectorGUI( Take target ) {

            Note( "Index " + ( target.transform.GetSiblingIndex( ) + 1 ));
            Space( );

            Expose( "type" );

            if( target.type == TakeType.scene ) {

                Expose( "scene" );
                Expose( "hideMediaControls" );
            }
            else if( target.type == TakeType.document ) {

                Expose( "document" );
            }
            else if( target.type == TakeType.video ) {

                Expose( "video" );
                Expose( "hideMediaControls" );
                Expose( "haltAfterPlayback" );
            }
            


            var isIdentification = target.type == TakeType.identification;

            if( isIdentification ) {

                target.hasConfirmation = false;
                target.hasJump = false;

                Expose( "hasBackground" );
                if( target.hasBackground ) Expose( "background" );
            }

            var isOrderList = target.type == TakeType.orderList;

            if( isOrderList ) {

                var initialText = "Order items";
                var info = initialText + ":\n";

                info += " piece | designation | dimensions | material number\n";

                var items = target.GetOrderItems( );

                foreach( var item in items) {

                    info += " " + item.piece + " | " + item.designation + " | " + item.dimensions + " | " + item.materialNumber + "\n";
                }

                Note( items.Length > 0 ? info.Trim( ) : "No " + initialText );
                Space( );
            }

            if( ! target.hasJump && ! isIdentification && ! isOrderList ) Expose( "hasConfirmation" );
            else target.hasConfirmation = false;
            
            if( target.hasConfirmation ) {

                Expose( "message" );
                Expose( "isConfirmationExclusive", "Is Exclusive" );
                ShowChildrenList( "Protocol Toggles", target.GetProtocolToggles( ));
                ShowChildrenList( "Protocol Inputs", target.GetProtocolInputs( ));
                Space( );
            }
            
            if( ! target.hasConfirmation && ! isIdentification && ! isOrderList ) Expose( "hasJump" );
            else target.hasJump = false;

            if( target.hasJump ) {

                Expose( "message" );
                ShowChildrenList( "Jump Toggles", target.GetJumpToggles( ));
                Space( );
            }

            if( target.hasJump || target.hasConfirmation ) {

                Expose( "dialogPosition" );
                Expose( "overrideToggleColor" );

                if( target.overrideToggleColor ) {

                    Expose( "toggleColor" );
                }

                Expose( "overrideToggleSize" );

                if( target.overrideToggleSize ) {

                    Expose( "toggleSize" );
                }
            }

            Expose( "isImportant" );
            
            ShowChildrenList( "Additional Documents", target.GetDocuments( ));

            if( ! target.hasJump ) Expose( "overrideNext" );

            if( target.overrideNext ) {

                Expose( "next" );
            }

            Expose( "hasForwardTake" );

            if( target.hasForwardTake ) {

                Expose( "forwardTake" );
            }

            Expose( "hasRewindTake" );

            if( target.hasRewindTake ) {

                Expose( "rewindTake" );
            }

            Expose( "hasOverlayText" );

            if( target.hasOverlayText ) {

                Expose( "overlayText" );
                Expose( "overlayTextPosition" );
            }

            Expose( "isChapter" );

            if( target.isChapter ) {

                Expose( "chapterDescription" );
            }
        }
    }
    
#endif

}