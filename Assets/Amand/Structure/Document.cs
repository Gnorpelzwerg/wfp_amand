using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using SmartEditor;
using UnityEngine.Video;

namespace Amand.Structure {

public enum DocumentType { text, image, reference, audio, scene, video, tableOfContents };

public class Document : SelfReferenceable< Document > {
    
    public DocumentType type;
    [TextArea( 1, 1000 )]
    public string text;
    public Sprite sprite;
    public bool hasReferenceText;
    public bool hasReferenceImage;
    public bool isFullScreenImage;
    public Text referenceText;
    public Image referenceImage;
    public bool hasName;
    public string displayName;
    public new AudioClip audio;
    public Well.Scene scene;
    public VideoClip video;

    public override bool IsReference( ) { return type == DocumentType.reference; }
}

#if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( Document ))]
    public class DocumentInspector : SelfReferenceableInspector< Document > {

        protected override void OnSelfReferenceableInspectorGUI( Document target ) {

            Expose( "type" );

            if( target.type == DocumentType.text || target.type == DocumentType.tableOfContents ) {

            	Expose( "text" );
            	Expose( "hasReferenceText", "Has Style Reference" );

            	if( target.hasReferenceText ) {

            		Expose( "referenceText", "Style Referencee" );
            	}
            }
            else if( target.type == DocumentType.image ) {

                Expose( "sprite" );
                Expose( "isFullScreenImage", "Full Screen" );

                //if we ever need fill-type full screen:
                //https://discussions.unity.com/t/how-can-i-make-an-image-maintain-its-aspect-ratio-in-unity-ui/182436

                if( ! target.isFullScreenImage ) {

                    Expose( "hasReferenceImage", "Has Style Reference" );

                    if( target.hasReferenceImage ) {

                        Expose( "referenceImage", "Style Reference" );
                    }
                }
            }
            else if( target.type == DocumentType.audio ) {

                Expose( "audio" );
            }
            else if( target.type == DocumentType.scene ) {

                Expose( "scene" );
            }
            else if( target.type == DocumentType.video ) {

                Expose( "video" );
            }

            Expose( "hasName" );
            if( target.hasName ) Expose( "displayName", "Name" );
        }
    }
    
#endif

}