using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand.Structure {

[System.Serializable]
public class ButtonSettings {
    
    public Sprite sprite;
    public AudioClip audio;
}


}
