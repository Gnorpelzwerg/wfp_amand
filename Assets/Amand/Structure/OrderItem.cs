using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using SmartEditor;

namespace Amand.Structure {


public class OrderItem : MonoBehaviour {

	public int piece = 1;
    public string designation;
    public string dimensions = "";
    public string materialNumber;
    public bool hasImage;
    public bool hasScene;
    public Sprite image;
    public Well.Scene scene;
}

#if UNITY_EDITOR

    [CanEditMultipleObjects]
    [CustomEditor( typeof( OrderItem ))]
    public class OrderItemInspector : SmartEditor< OrderItem > {

        protected override void OnSmartInspectorGUI( OrderItem target ) {

            Expose( "piece" );
            Expose( "designation" );
            Expose( "dimensions" );
            Expose( "materialNumber" );

            Expose( "hasImage" );

            if( target.hasImage ) {

                Expose( "image" );
            }

            Expose( "hasScene" );

            if( target.hasScene ) {

                Expose( "scene" );
            }
        }
    }

#endif


}
