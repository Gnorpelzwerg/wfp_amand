using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SmartEditor;

namespace Amand.Structure {


public class ProtocolInput : MonoBehaviour {

    public Vector2 position = new Vector2( 0.5f, 0.5f );
    public Vector2 pixelOffset = new Vector2( 0, 0 );
    public int width = 10;
    public string identifier = "None";
    public string description = "";
    public bool overridePlaceholder = false;
    public string placeholder = "";
    public bool overridePadding = false;
    public int padding = 10;
    public bool autoJumpOnLimit = false;
    public bool suggestInputs = false;
    public bool overrideColor = false;
    public Color fieldColor = Color.grey;
}


#if UNITY_EDITOR

    [CanEditMultipleObjects]
    [CustomEditor( typeof( ProtocolInput ))]
    public class ProtocolInputInspector : SmartEditor< ProtocolInput > {

        protected override void OnSmartInspectorGUI( ProtocolInput target ) {

            Expose( "position" );
            Expose( "pixelOffset" );
            Expose( "identifier" );
            Expose( "width" );
            Expose( "description" );
            Expose( "overridePadding" );

            if( target.overridePadding ) {

                Expose( "padding" );
            }

            Expose( "overridePlaceholder" );

            if( target.overridePlaceholder ) {

                Expose( "placeholder" );
            }

            Expose( "autoJumpOnLimit" );
            Expose( "suggestInputs" );

            Expose( "overrideColor" );

            if( target.overrideColor ) {

                Expose( "fieldColor" );
            }
        }
    }

#endif

}