using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class AmandNotification : MonoBehaviour {
    
    public Notification parent;
    public CanvasGroup group;

    void Update( ) {

        group.alpha = parent.health;
    }
}


}
