using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Linq;

namespace Amand {


public class RenderImage : MonoBehaviour {
    
    public Image image;
    public Material renderMaterialPrefab;
    public RenderTexture texture;
    public Material renderMaterial;
    public Material webCamMaterial;
    
    public Camera currentCamera;
    public WebCamTexture webCamTexture;

    bool initialized = false;

    public void Detach( ) {

        if( ! initialized ) Init( );

        foreach( var cam in FindObjectsOfType< Camera >( )) {

            if( cam.targetTexture == texture ) cam.targetTexture = null;
        }

        foreach( var player in FindObjectsOfType< VideoPlayer >( )) {

            if( player.targetTexture == texture ) player.targetTexture = null;
        }

        webCamTexture.Stop( );
    }

    void Start( ) {

        Init( );
    }

    void Init( ) {

        initialized = true;
        string name = null;
        var all = WebCamTexture.devices;
        var candidates = all.Where( d => ! d.isFrontFacing ).ToArray( );
        if( candidates.Length > 0 ) name = candidates[ 0 ].name;
        else if( all.Length > 0 ) name = all[ 0 ].name;

        webCamTexture = new WebCamTexture( name, Screen.width, Screen.height, 60 );
        texture = new RenderTexture( Screen.width, Screen.height, 24 );
        texture.antiAliasing = 2;
        renderMaterial = Instantiate( renderMaterialPrefab );
        renderMaterial.SetTexture( "_MainTex", texture );
    }

    public void Disable( ) {

        if( ! initialized ) Init( );
        image.enabled = false;
        webCamTexture.Stop( );
    }

    public void Attach( Camera cam ) {

        if( ! initialized ) Init( );
        Detach( );
        image.enabled = true;
        currentCamera = cam;
        cam.targetTexture = texture;
        image.material = renderMaterial;
    }

    public void AttachVideo( VideoPlayer player ) {

        if( ! initialized ) Init( );
        Detach( );
        image.enabled = true;
        player.targetTexture = texture;
        image.material = renderMaterial;
    }

    public void AttachPhoto( ) {

        if( ! initialized ) Init( );
        Detach( );
        image.enabled = true;
        webCamTexture.Play( );
        var mat = new Material( webCamMaterial );
        mat.mainTexture = webCamTexture;
        image.material = mat;
    }
}


}