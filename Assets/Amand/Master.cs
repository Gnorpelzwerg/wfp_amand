using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Amand.Structure;
using System.Linq;
using SimpleJSON;
using System.IO;
using UnityEngine.Networking;
using System.Net.Http;
using System;
using System.Text;
using System.Security.Cryptography;

namespace Amand {


public class Record {

    public Take take;
}


public class Master : MonoBehaviour {

	private static Master instance;
	public static HashAlgorithm hashAlgorithm = MD5.Create( );

	static byte[ ] GetHash( string inputString ){

        return hashAlgorithm.ComputeHash( Encoding.UTF8.GetBytes( inputString ));
    }

    public static string Hash( string inputString ) {

        StringBuilder sb = new StringBuilder();
        foreach (byte b in GetHash(inputString))
            sb.Append(b.ToString("X2"));

        return sb.ToString();
    }

	public static Master Get( ) { return instance; }
    
	void Awake( ) { instance = this; }

	public string version;
	public GameObject titleScreen;
	public Menu menu1;
	public Menu menu2;
	public Menu menu3;
	public GameObject mainScreen;
	public GameObject structure;
	
	public MenuButton menu1ButtonPrefab;
	public Text menu1Info;
	public Transform menu1Content;

	public MenuButton menu2ButtonPrefab;
	public Text menu2Info;
	public Transform menu2Content;
	
	public MenuButton menu3ButtonPrefab;
	public Text menu3Info;
	public Transform menu3Content;
	public Scrollbar menu3Scrollbar;
	public ScrollRect menu3ScrollRect;
	
	public Text mainScreenTitle;
	public Transform overviewContent;
	public TakeButton takeButtonPrefab;
	public GameObject background; 
	public GameObject leftArrow;
	public GameObject rightArrow;
	public ScreenContent screenContentA;
	public ScreenContent screenContentB;
	public Transform documentParent;
	public DocumentButton documentButtonPrefab;
	public GameObject closeButton;
	public ScrollRect overviewScrollRect;
	public Text overlayText;
	public BoundsPosition overlayTextPosition;
	public AudioSource defaultAudioSource;
	public AudioSource confirmAudioSource;
	public AudioSource menuBackAudioSource;
	public AudioSource tickAudioSource;
	public AudioSource cleanAudioSource;
	public DocumentAudioSource documentAudioSource;
	public GameObject photoButton;
	public Transform notificationsParent;
	public Notification notificationPrefab;
	public Document tableOfContents;
	public GameObject forwardArrow;
	public GameObject rewindArrow;
	[Space( 10 )]
	public AudioClip launch;
	public AudioClip confirm;
	public AudioClip menu1Sound;
	public AudioClip menu2Sound;
	public AudioClip menu3Sound;
	public AudioClip menuTap;
	public AudioClip home;
	public AudioClip takeTap;
	public AudioClip menuBackSound;
	public AudioClip documentOpen;
	public AudioClip documentClose;
	public AudioClip toggledOnSound;
	public AudioClip toggledOffSound;
	public AudioClip tick;

	[Space( 20 )]
	public Structure.Model model;
	public Structure.PerformanceType performanceType;
	public Structure.Performance performance;
	public int takeIndex;
	public Stack< Record > history = new Stack< Record >( );
	public int previousTakeIndex;
	public int menuIndex;
	public float lastMenuTime;

	public ScreenContent activeScreenContent;
	public float lastCheck = 0;
	public HashSet< string > savedUids = new HashSet< string >( );
	public HashSet< string > sentUids = new HashSet< string >( );
	public string currentUid = "dummy";
	public string sessionUid;
	public int fileCounter = 0;

	public bool IsSaved( ) {

		return savedUids.Contains( currentUid );
	}

	public bool IsSent( ) {

		return sentUids.Contains( currentUid );
	}

	string BufferPath( ) {

		return Path.Combine( Application.persistentDataPath, "buffer" );
	}

	string MetaPath( ) {

		return Path.Combine( Application.persistentDataPath, "meta" );
	}

	string KnownValuePath( string key ) {

		return Path.Combine( Application.persistentDataPath, $"known_values_of_{ key }.json" );
	}

	void Start( ) {

		sessionUid = Guid.NewGuid( ).ToString( );
		SetTitleScreen( );
		SoftwareLayerSwitch.MaskNothing( );
		System.IO.Directory.CreateDirectory( BufferPath( ));
		System.IO.Directory.CreateDirectory( MetaPath( ));
	}

	void PlaySound( AudioClip clip, AudioSource source = null ) {

		source ??= defaultAudioSource;
		if( clip != null ) source.PlayOneShot( clip );
	}

	public void PlayButtonSound( AudioClip clip ) {

		PlaySound( clip, cleanAudioSource );
	}

	void SetMenuIndex( int newIndex ) {

		menu1.gameObject.SetActive( true );
		menu2.gameObject.SetActive( true );
		menu3.gameObject.SetActive( true );

		menu1.SetImmediateZ( 1 - menuIndex );
		menu1.SetTargetZ( 1 - newIndex );
		menu2.SetImmediateZ( 2 - menuIndex );
		menu2.SetTargetZ( 2 - newIndex );
		menu3.SetImmediateZ( 3 - menuIndex );
		menu3.SetTargetZ( 3 - newIndex );

		menuIndex = newIndex;
	}

	void SetTitleScreen( ) {

		titleScreen.SetActive( true );
		menuIndex = 0;
		SetMenuIndex( 0 );
		mainScreen.SetActive( false );
		ResetMenuTime( );
		PlaySound( launch, cleanAudioSource );

		documentAudioSource.Stop( );
		SoftwareLayerSwitch.MaskNothing( );
	}

	void SetMenu1( ) {

		model = null;
		performanceType = null;
		performance = null;
		takeIndex = -1;
		history.Clear( );
		GetLatest( );

		ClearChildren( menu1Content );

		foreach( Transform child in structure.transform ) {

			var model = child.GetComponent< Structure.Model >( );
			var button = Instantiate( menu1ButtonPrefab, menu1Content );
			button.titleTexts[ 0 ].text = button.titleTexts[ 1 ].text = model.displayName;
			button.menuName = "1";
			button.selection = model;
			button.ImplementSettings( model.buttonSettings );
		}

		menu1.Initialize( );
		titleScreen.SetActive( false );
		SetMenuIndex( 1 );
		mainScreen.SetActive( false );
		ResetMenuTime( );
		PlaySound( menu1Sound );
		PlaySound( home );

		documentAudioSource.Stop( );
		SoftwareLayerSwitch.MaskNothing( );
	}

	void SetMenu2( ) {

		performanceType = null;
		performance = null;
		takeIndex = -1;
		history.Clear( );

		menu2Info.text = $"<b>{ model.displayName }</b> 〉 Bitte wählen Sie eine Vorgangskategorie aus";
		ClearChildren( menu2Content );

		foreach( Transform child in model.transform ) {

			var performanceType = child.GetComponent< Structure.PerformanceType >( );
			var button = Instantiate( menu2ButtonPrefab, menu2Content );
			button.titleTexts[ 0 ].text = button.titleTexts[ 1 ].text = performanceType.displayName;
			button.menuName = "2";
			button.selection = performanceType;
			button.ImplementSettings( performanceType.buttonSettings );
		}

		menu2.Initialize( );
		titleScreen.SetActive( false );
		SetMenuIndex( 2 );
		mainScreen.SetActive( false );
		ResetMenuTime( );
		PlaySound( menu2Sound );

		documentAudioSource.Stop( );
		SoftwareLayerSwitch.MaskNothing( );
	}

	void SetMenu3( ) {

		performance = null;
		takeIndex = -1;
		history.Clear( );

		menu3Info.text = $"<b>{ model.displayName }</b> 〉 <b>{ performanceType.displayName }</b> 〉 Bitte wählen Sie einen Vorgang aus";
		ClearChildren( menu3Content );

		foreach( Transform child in performanceType.transform ) {

			var performance = child.GetComponent< Structure.Performance >( );
			var button = Instantiate( menu3ButtonPrefab, menu3Content );
			button.titleTexts[ 0 ].text = button.titleTexts[ 1 ].text = performance.displayName;
			button.menuName = "3";
			button.selection = performance;
			button.ImplementSettings( performance.buttonSettings );
		}

		menu3.Initialize( );
		titleScreen.SetActive( false );
		SetMenuIndex( 3 );
		mainScreen.SetActive( false );
		ResetMenuTime( );
		PlaySound( menu3Sound );
		Invoke( "Menu3UpdateScrollbar", 0.2f );

		documentAudioSource.Stop( );
		SoftwareLayerSwitch.MaskNothing( );
	}

	void Menu3UpdateScrollbar( ) {

		menu3Scrollbar.value = 0.01f;
	}

	void SetMainScreen( ) {

		titleScreen.SetActive( false );
		mainScreen.SetActive( true );

		takeIndex = -1;
		
		mainScreenTitle.text = $"<b>{ model.displayName }</b> 〉<b>{ performanceType.displayName }</b> 〉 { performance.displayName.Replace( "-\n", "" ).Replace( "\n", " " )}";
		ClearChildren( overviewContent );

		var takes = GetTakes( );
		for( int i = 0; i < takes.Count; ++ i ) {

			var button = Instantiate( takeButtonPrefab );
			button.transform.SetParent( overviewContent );
			button.transform.localScale = Vector3.one;
			button.label.text = "" + ( i + 1 );
			button.takeIndex = i;
			button.take = takes[ i ];
		}

		activeScreenContent = null;
		SoftwareLayerSwitch.MaskNothing( );
		SetTake( 0 );
		SetMenuIndex( 4 );
		ResetMenuTime( );

		documentAudioSource.Stop( );
	}

	void SetTake( int newIndex ) {

		if( newIndex == takeIndex ) return;
		PushHistory( );

		var previousTakeIndex = takeIndex;
		takeIndex = newIndex;

		SoftwareLayerSwitch.MaskNothing( );

		var take = GetCurrentTakeData( );

		if( activeScreenContent ) {

			activeScreenContent.GetComponent< Animator >( ).SetTrigger( previousTakeIndex < takeIndex ? "fade out left" : "fade out right" );
		}
		
		SwitchScreenContent( );
		activeScreenContent.GetComponent< Animator >( ).SetTrigger( previousTakeIndex < takeIndex ? "fade in right" : "fade in left" );
		SetScreenContent( take );

		ClearChildren( documentParent );

		var documents = new List< Structure.Document >( take.GetDocuments( ));
		
		if( performance.hasTableOfContents ) {

			tableOfContents.text = performance.GetTableOfContents( );
			documents.Add( tableOfContents );
		}

		foreach( var document in documents.ToArray( )) {

			var button = Instantiate( documentButtonPrefab, documentParent );
			button.document = document;
			button.MarkActive( false );
		}

		closeButton.SetActive( false );
		currentUid = $"{ sessionUid }_{ fileCounter ++ }.json";

		overlayText.transform.parent.gameObject.SetActive( take.hasOverlayText );
		overlayText.text = take.overlayText;
		overlayTextPosition.positionInBounds = take.overlayTextPosition;

		var group = overviewContent.GetComponent< HorizontalLayoutGroup >( );
		var count = GetTakes( ).Count;
		var rt = takeButtonPrefab.GetComponent< RectTransform >( );
		var width = group.padding.left + group.padding.right + ( count - 1 ) * group.spacing + ( count ) * rt.rect.width;
		var positionX = group.padding.left + takeIndex * group.spacing + ( takeIndex + 0.5f ) * rt.rect.width;
		
		//this has to match the reference horizontal resolution
		var screenWidth = 2000f;

		var W = width;
		var t = screenWidth / 2;
		var x = positionX;
		
		float y = 0;
		if( x < t ) y = 0;
		else if( x > W - t ) y = 1;
		else y = ( x - t ) / ( W - 2 * t );

		overviewScrollRect.normalizedPosition = new Vector2( y, 0 );
		
		documentAudioSource.Stop( );
		photoButton.SetActive( take.type == Structure.TakeType.photo );
	}

	void SetScreenContent( Take take ) {

		if( take.type == Structure.TakeType.scene ) {

			activeScreenContent.SetScene( take.scene );
		}
		else if( take.type == Structure.TakeType.document ) {

			activeScreenContent.SetDocument( take.document );
		}
		else if( take.type == Structure.TakeType.video ) {

			activeScreenContent.SetVideo( take.video );
		}
		else if( take.type == Structure.TakeType.photo ) {

			activeScreenContent.SetPhoto( );
		}
		else if( take.type == Structure.TakeType.identification ) {

			activeScreenContent.SetIdentification( );
		}
		else if( take.type == Structure.TakeType.orderList ) {

			activeScreenContent.SetOrderList( );
		}
		else {

			Debug.LogError( $"take type { take.type } not implemented" );
			activeScreenContent.SetEmpty( );
		}
	}

	void PushHistory( ) {

		if( takeIndex != -1 && takeIndex != 1000 ) { //just some large number

			var record = new Record( );
			record.take = GetCurrentTake( );
			history.Push( record );	
		}
	}

	Structure.Take GetCurrentTake( ) { return GetTakes( )[ takeIndex ]; }

	public Structure.Take GetCurrentTakeData( ) { return GetCurrentTake( ).Follow( ); }

	List< Structure.Take > GetTakes( ) {

		return performance.GetTakes( );
	}

	public static void ClearChildren( Transform parent ) {

		while( parent.childCount > 0 ) {

			var child = parent.GetChild( 0 );
			child.SetParent( null );
			Destroy( child.gameObject );
		}
	}

	void SwitchScreenContent( ) {

		activeScreenContent = activeScreenContent == screenContentA ? screenContentB : screenContentA;
	}

	void ResetMenuTime( ) {

		lastMenuTime = Time.time;
	}

	bool CanInteract( ) {

		return Time.time - lastMenuTime >= 0.5f;
	}

	public void OnMenu( string menuName, UnityEngine.Object selection, string primitiveSelection ) {

		if( ! CanInteract( )) return;
		PlaySound( menuTap );

		switch( menuName ) {

			case "1": OnMenu1( selection as Model ); return;
			case "2": OnMenu2( selection as PerformanceType ); return;
			case "3": OnMenu3( selection as Performance ); return;
		}
	}

	void OnMenu1( Structure.Model selection ) {
		
		model = selection;
		SetMenu2( );
	}

	void OnMenu2( Structure.PerformanceType selection ) {
		
		performanceType = selection;
		SetMenu3( );
	}

	void OnMenu3( Structure.Performance selection ) {

		performance = selection;
		SetMainScreen( );
	}

	public void Next( ) {

		var take = GetCurrentTake( );
		if( take.overrideNext ) {

			JumpToTake( take.next );
		}
		else if( take.hasJump ) {

			OnJump( );
		}
		else {

			SetTake( takeIndex + 1 );			
		}

		PlaySound( takeTap );
	}

	void JumpToTake( Take target ) {

		performanceType = target.transform.parent.parent.GetComponent< Structure.PerformanceType >( );

		var jumpToPerformance = target.transform.parent.GetComponent< Structure.Performance >( );
		if( jumpToPerformance != performance ) {

			performance = jumpToPerformance;
			PushHistory( );
			SetMainScreen( );
		}

		var jumpToIndex = target.transform.GetSiblingIndex( );
		SetTake( jumpToIndex );
	}

	public void Previous( ) {

		var prev = history.Pop( );
		//this becomes previousTakeIndex so we always scroll left
		takeIndex = 1000; 
		JumpToTake( prev.take );
		PlaySound( takeTap );
	}

	public void Forward( ) {

		var take = GetCurrentTake( );
		JumpToTake( take.forwardTake );
		PlaySound( takeTap );
	}

	public void Rewind( ) {

		var take = GetCurrentTake( );
		JumpToTake( take.rewindTake );
		PlaySound( takeTap );
	}

	public void OnTake( int index ) {

		//we can only skip forward
		if( index >= takeIndex ) SetTake( index );
		PlaySound( takeTap );
	}

	public void OnHome( ) {

		if( ! CanInteract( )) return;
		if( menuIndex == 1 ) return; //is already home
		SetMenu1( );
	}

	public void OnDocument( Structure.Document document ) {

		if( document.type == DocumentType.audio ) documentAudioSource.Play( document.audio );
		else PlaySound( documentOpen );
		activeScreenContent.SetDocument( document );
		closeButton.SetActive( true );

		foreach( var button in FindObjectsOfType< DocumentButton >( )) {

			button.MarkActive( button.document == document );
		}
	}

	public void OnClose( ) {

		var take = GetCurrentTake( );
		SetScreenContent( take );

		closeButton.SetActive( false );

		foreach( var button in FindObjectsOfType< DocumentButton >( )) {

			button.MarkActive( false );
		}

		PlaySound( documentClose );
		documentAudioSource.Stop( );
	}

	void Update( ) {

		if( Input.GetKeyDown( KeyCode.Escape )) {

			if( CanInteract( )) {

				if( closeButton.activeInHierarchy ) OnClose( );
				else {

					if( menuIndex == 2 ) SetMenu1( );
					else if( menuIndex == 3 ) SetMenu2( );
					else if( menuIndex == 4 ) SetMenu3( );

					PlaySound( menuBackSound, menuBackAudioSource );
				}
			}
		}

		if( Time.time - lastCheck > 5 ) {

			lastCheck = Time.time;
			var files = Directory.GetFiles( BufferPath( )).Select( Path.GetFileName );
			var metaFiles = Directory.GetFiles( MetaPath( )).Select( Path.GetFileName );
			
			foreach( var fileName in files ) {

				if( metaFiles.Contains( fileName )) {

					var info = ReadJSONFile( Path.Combine( MetaPath( ), fileName )) as JSONObject;

					if( info[ "type" ] == "email" ) {

						SendEmail( fileName, info );
					}
					else {

						TryUpload( fileName );
					}
				}
				else {

					TryUpload( fileName );
				}
			}
		}

		if( menuIndex == 4 ) {

			var take = GetCurrentTakeData( );
			leftArrow.SetActive( history.Count > 0 );

			var hasNextTake = take.overrideNext || takeIndex < GetTakes( ).Count - 1;
			var userCannotSkip = take.hasJump || ( take.hasConfirmation && ! IsSaved( ));
			rightArrow.SetActive( ! userCannotSkip && hasNextTake );	

			forwardArrow.SetActive( take.hasForwardTake );
			rewindArrow.SetActive( take.hasRewindTake );
		}
	}

	void TryUpload( string uid ) {

		//only for debugging purposes
		#if UNITY_EDITOR

		StartCoroutine( Upload( uid ));

		#else

		UploadEmail( uid );

		#endif
	}

	async void SendEmail( string uid, JSONObject info ) {

        var success = false;
        var errorMessage = "";

        try {

		Debug.Log( $"sending email '{ info[ "subject" ]}' to { info[ "to" ]}" );
		var result = await SendEmailRaw( info[ "to" ], info[ "subject" ], File.ReadAllText( Path.Combine( BufferPath( ), uid )), null );
		if( result.IsSuccessStatusCode ) success = true;
		else errorMessage = result.ReasonPhrase;
	}
	catch( Exception e ) {

		errorMessage = e.Message;
	}

        if( ! success ) {

		Notify( $"mail delivery <color=#faa><b>failed!</b></color> { errorMessage }" );
        }
        else {

		Debug.Log( $"mail delivered: { uid }" );
		PlaySound( confirm, confirmAudioSource );
		File.Delete( Path.Combine( BufferPath( ), uid ));
		File.Delete( Path.Combine( MetaPath( ), uid ));
		Notify( $"mail delivered" );
        }
	}

	async void UploadEmail( string uid ) {

		var path = Path.Combine( BufferPath( ), uid );
		var isImage = path.Contains( ".png" );
        var formData = new List< IMultipartFormSection >( );
        formData.Add( new MultipartFormFileSection( "", File.ReadAllBytes( path ), uid, isImage ? "image/png" : "text/plain" ));

        var success = false;
        var errorMessage = "";
        string processName = isImage ? "photo upload" : "server confirmation";

        try {

		var result = await SendEmailRaw( null, null, message, null );
        	if( result.IsSuccessStatusCode ) success = true;	
        	else errorMessage = result.ReasonPhrase;
       	}
       	catch( Exception e ) {

       		errorMessage = e.Message;
       	}

        if( ! success ) {

		Notify( $"{ processName } <color=#faa><b>failed!</b></color> { errorMessage }" );
        }
        else {

        	Debug.Log( $"upload successful: { uid }" );
        	PlaySound( confirm, confirmAudioSource );
        	sentUids.Add( uid );
        	File.Delete( Path.Combine( BufferPath( ), uid ));
		if( isImage ) Notify( $"{ processName } done" );
        }
    }

    async System.Threading.Tasks.Task< System.Net.Http.HttpResponseMessage > SendEmailRaw( string to, string subject, string message, string headers ) {

		var client = new HttpClient( );
        var dict = new Dictionary< string, string >( ) {{ "to", to }, { "subject", subject }, { "message", message }, { "headers", headers }};

        try {

		var task = client.PostAsync( "https://wfp-design.com/vem/mail/index.php", new FormUrlEncodedContent( dict ));
		return await task;
	}
	catch( Exception ) {

		throw;
	}
    }

    IEnumerator Upload( string uid ) {

		var path = Path.Combine( BufferPath( ), uid );
		var isImage = path.Contains( ".png" );
        var formData = new List< IMultipartFormSection >( );
        formData.Add( new MultipartFormFileSection( "", File.ReadAllBytes( path ), uid, isImage ? "image/png" : "text/plain" ));

        var r = UnityWebRequest.Post( "http://wfp-design.com/vem/upload.php", formData );
        yield return r.SendWebRequest( );

        string processName = isImage ? "photo upload" : "server confirmation";
        if( r.result != UnityWebRequest.Result.Success ) {

		Notify( $"{ processName } <color=#faa><b>failed!</b></color> { r.error }" );
		Debug.Log( r.error );
        }
        else {

		Debug.Log( $"upload successful: { uid }" );
		PlaySound( confirm, confirmAudioSource );
		sentUids.Add( uid );
		File.Delete( Path.Combine( BufferPath( ), uid ));
		if( isImage ) Notify( $"{ processName } done" );
        }
    }

	public void OnConfirm( string description, string toggleInfo, JSONObject inputInfo ) {

		var uid = currentUid;
		var n = new JSONObject( );
		n[ "description" ] = description;
		n[ "toggleInfo" ] = toggleInfo;
		n[ "model" ] = model.displayName;
		n[ "inputInfo" ] = inputInfo;
		n[ "performanceType" ] = performanceType.displayName;
		n[ "performance" ] = performance.displayName;
		n[ "date_utc" ] = DateTime.UtcNow.ToString( "o", System.Globalization.CultureInfo.InvariantCulture );
		n[ "uid" ] = uid;
		n[ "session" ] = sessionUid;
		n[ "version" ] = version;

		CommitLog( uid, n.ToString( ));
		lastCheck = 0;
	}

	public void OnPhoto( ) {

		var texture = activeScreenContent.renderImage.webCamTexture;
		texture.Pause( );
		var t = new Texture2D( texture.width, texture.height );
		t.SetPixels( texture.GetPixels( ));
		texture.Play( );
		t.Apply( );
		var data = t.EncodeToPNG( );
		var name = $"{ sessionUid }_{ fileCounter ++ }.png";
		File.WriteAllBytes( Path.Combine( BufferPath( ), name ), data );
		lastCheck = 0;
	}

	public void CommitEmail( string to, string subject, string message ) {

		Debug.Log( $"commit email '{ subject }' to { to }" );
		var name = $"{ sessionUid }_{ fileCounter ++ }.txt";
		var info = new JSONObject( );
		info[ "type" ] = "email";
		info[ "to" ] = to;
		info[ "subject" ] = subject;
		WriteJSONFile( Path.Combine( MetaPath( ), name ), info );

		File.WriteAllText( Path.Combine( BufferPath( ), name ), message );
		lastCheck = 0;
	}

	void CommitLog( string uid, string data ) {

		File.WriteAllText( Path.Combine( BufferPath( ), uid ), data );
		Debug.Log( $"commit successful: { uid }" );
		savedUids.Add( uid );
	}

	public void OnJump( ) {

		foreach( Transform child in activeScreenContent.toggleButtonParent ) {

			var toggleButton = child.GetComponent< ToggleButton >( );
			if( toggleButton.isOn( )) {

				JumpToTake(( toggleButton.toggle as JumpToggle ).jumpTo );
				break;
			}
		}

		PlaySound( takeTap );
	}

	//just for audio feedback
	public void OnToggleButtonChange( bool newValue ) {

		PlaySound( newValue ? toggledOnSound : toggledOffSound, confirmAudioSource );
	}

	//just for audio feedback
	public void OnMenuButtonPointerEnter( ) {

		if( Application.platform != RuntimePlatform.Android ) PlaySound( tick, tickAudioSource );
	}

	public void OnVersion( string versionInfo ) {

		menu1Info.text = versionInfo;
	}

	void GetLatest( ) {

    	menu1Info.text = $"v{ version }";
    	menu1Info.alignment = TextAnchor.MiddleRight;

        StartCoroutine( GetLatestInternal( ));
    }

    IEnumerator GetLatestInternal( ) {

        var r = UnityWebRequest.Get( $"http://wfp-design.com/amand/latest.php" );
        yield return r.SendWebRequest( );

        if( r.result != UnityWebRequest.Result.Success ) {

            Debug.Log( r.error );
        }
        else {

            var info = JSON.Parse( r.downloadHandler.text ) as JSONObject;
            string otherVersion = info[ "name" ];
            otherVersion = otherVersion.Split( new string[ ]{ ".apk" }, StringSplitOptions.None )[ 0 ];

            if( String.Compare( version, otherVersion ) < 0 ) {

            	menu1Info.text = $"Update verfügbar auf <b>wfp-design.com/amand</b> ({ version } → { otherVersion })";
            	menu1Info.alignment = TextAnchor.MiddleLeft;
            }
            else if( String.Compare( version, otherVersion ) > 0 ) {

            	menu1Info.text = $"Entwicklungs-Version (Version { version })";
            	menu1Info.alignment = TextAnchor.MiddleLeft;
            }
            else {

            	menu1Info.text = $"Software ist auf dem aktuellen Stand (Version { version })";
            	menu1Info.alignment = TextAnchor.MiddleLeft;
            }
        }
    }

    public void VisitWebsite( ) {

    	Application.OpenURL( "http://wfp-design.com/amand" );
    }

    public void Notify( string text ) {

    	var notification = Instantiate( notificationPrefab, notificationsParent );
    	notification.GetComponentsInChildren< Text >( )[ 0 ].text = text;
    }

    void WriteJSONFile( string path, JSONNode node ) {

    	File.WriteAllText( path, node.ToString( ));
    }

    JSONNode ReadJSONFile( string path ) {

    	return JSON.Parse( File.ReadAllText( path ));
    }

    public string[ ] GetKnownValues( string key ) {

	if( ! File.Exists( KnownValuePath( key ))) SetKnownValues( key, new string[ ]{ });
	return ( ReadJSONFile( KnownValuePath( key )) as JSONArray ).Children.Select( name => name.Value ).ToArray( );
    }

    public void SetKnownValues( string key, string[ ] values ) {

    	var array = new JSONArray( );
	foreach( var id in values ) array.Add( id );
	WriteJSONFile( KnownValuePath( key ), array );
    }
}


}