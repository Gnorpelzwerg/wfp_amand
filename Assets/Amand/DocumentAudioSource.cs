using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Amand {


public class DocumentAudioSource : Animatable< Vector2 > {
    
    public AudioSource sourceA;
    public AudioSource sourceB;

    public AudioSource active;

    void Start( ) {

    	SetImmediate( Vector2.zero );
    }

    void Update( ) {

    	PlayOneFrame( Time.deltaTime );
    }

    public void Play( AudioClip clip ) {

    	active = active == sourceA ? sourceB : sourceA;
    	active.clip = clip;
    	active.Stop( );
    	active.Play( );

    	SetTarget( active == sourceA ? new Vector2( 1, 0 ) : new Vector2( 0, 1 ), true );
    }

    public void Stop( ) {

    	SetTarget( Vector2.zero, true );
    }

    protected override Vector2 GetAnimatedProperty( ) {

        return new Vector2( sourceA.volume, sourceB.volume );
    }

    protected override void SetAnimatedProperty( Vector2 target ) {

        sourceA.volume = target.x;
        sourceB.volume = target.y;
    }
}


}