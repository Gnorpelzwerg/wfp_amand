using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class ToggleButton : MonoBehaviour {
    
    public Image background;
    public Color colorOff;
    public Toggle uiToggle;
    public float startTime;

    [ Space( 5 )]
    public Structure.Toggle toggle;
    public ScreenContent parent;
    public Color colorOn;

    public bool isOn( ) {

        return uiToggle.isOn;
    }

    void Start( ) {

        startTime = Time.time;
        uiToggle.isOn = ( toggle is Structure.ProtocolToggle ) ? 
            ( toggle as Structure.ProtocolToggle ).isOnByDefault : transform.GetSiblingIndex( ) == 0;
    }

    void Update( ) {

        var target = toggle.target;
        var cam = parent.renderImage.currentCamera;
        var screenPosition = cam.WorldToScreenPoint( target.position );
        //transform.GetComponent< RectTransform >( ).anchoredPosition = screenPosition / new Vector2( cam.pixelWidth, cam.pixelHeight ) * new Vector2( 2000, 1200 );
        var rt = transform.GetComponent< RectTransform >( );
        rt.anchorMin = rt.anchorMax = screenPosition / new Vector2( cam.pixelWidth, cam.pixelHeight );
        rt.anchoredPosition = Vector2.zero;
        background.color = isOn( ) ? colorOn : colorOff;
    }

    public void ValueChanged( ) {

        if( Time.time - startTime < 0.5f || startTime == 0 ) return; //ignore script change
        Master.Get( ).OnToggleButtonChange( isOn( ));
    }
}


}
