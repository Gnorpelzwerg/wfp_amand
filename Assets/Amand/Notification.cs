using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class Notification : Animatable< float > {
    
    public float lifeTime = 3;

    [Space( 10 )]
    public float health;
    public float start;
    public RectTransform rt;
    public bool isDying;

    protected override void SetAnimatedProperty( float target ) {

        health = target;
        rt.SetSizeWithCurrentAnchors( RectTransform.Axis.Vertical, GetSize( ) * health );
    }

    protected override float GetAnimatedProperty( ) {

        return health;
    }

    protected override float Interpolate( float t, float a, float b ) {

        return Mathf.Lerp( a, b, t );
    }

    void Start( ) {

        rt = GetComponent< RectTransform >( );
        SetImmediate( 0 );
        SetTarget( 1 );
        start = Time.time;
    }

    void Update( ) {

        PlayOneFrame( Time.deltaTime );

        if( Time.time - start > lifeTime && ! isDying ) {

            isDying = true;
            SetTarget( 0 );
        }

        if( Time.time - start > lifeTime + 1 / _speed + 1 ) {

            transform.SetParent( null );
            Canvas.ForceUpdateCanvases( );
            Destroy( gameObject );
        }
    }

    float GetSize( ) {

        return transform.GetChild( 0 ).GetComponent< RectTransform >( ).rect.height;
    }
}
