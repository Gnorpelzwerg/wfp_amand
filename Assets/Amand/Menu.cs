using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

namespace Amand {

public class Menu : Utilities.Animatable< Vector3 > {
    
    public float z;
    public Transform content;
    public CanvasGroup canvasGroup;
    public Image tint;
    public Transform grid;
    public ScrollRect scrollRect;

    public void SetImmediateZ( float zIn ) {

        SetImmediate( Vector3.forward * Mathf.Clamp( zIn, -1, 1 ));
        foreach( var b in GetMenuButtons( )) b.SetImmediateZ( Mathf.Clamp( zIn, -1, 1 ));
    }

    public void SetTargetZ( float zIn ) {

        SetTarget( Vector3.forward * Mathf.Clamp( zIn, -1, 1 ), true );
        foreach( var b in GetMenuButtons( )) b.SetTargetZ( Mathf.Clamp( zIn, -1, 1 ));
    }

    protected override Vector3 GetAnimatedProperty( ) {

        return Vector3.forward * z;
    }

    protected override void SetAnimatedProperty( Vector3 target ) {

        var oldZ = z;
        z = target.z;
        var draw = z > -1f && z < 1f;
        
        canvasGroup.alpha = 1 - Mathf.Abs( z );
        tint.color = new Color( 0, 0, 0, 0.5f * Mathf.Abs( z ));
        canvasGroup.blocksRaycasts = Mathf.Abs( z ) < 0.1f;
        canvasGroup.interactable = Mathf.Abs( z ) < 0.1f;
    }

    MenuButton[ ] GetMenuButtons( ) {

        return content.GetComponentsInChildren< MenuButton >( ); //children
    }

    public void Initialize( ) {

        Canvas.ForceUpdateCanvases( );
        grid.GetComponent< GridLayoutHelper >( ).dirty = true;
        scrollRect.normalizedPosition = new Vector2( 0, 0 );
    }

    void Update( ) {

        PlayOneFrame( Time.deltaTime );
    }
}


}