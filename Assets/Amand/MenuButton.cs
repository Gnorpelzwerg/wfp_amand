using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Amand {


public class MenuButton : Utilities.Animatable< Vector3 >, IPointerEnterHandler {
    
    public string menuName;
    public Object selection;
    public string primitiveSelection;
    public Text[ ] titleTexts;
    public Image image;
    public Image border;
    public Color disabledColor;
    public CanvasGroup canvasGroup;
    public Vector3 localTargetCenter;

    public float z;
    public int counter = 0;
    public int counterThreshold = 3;
    public float startTime;
    public AudioClip audioClip;

    public void ImplementSettings( Structure.ButtonSettings settings ) {

        if( settings != null ) {

            if( settings.sprite != null ) image.sprite = settings.sprite;
            audioClip = settings.audio;    
        }
    }

    public void OnClick( ) {

        Master.Get( ).PlayButtonSound( audioClip );
        Master.Get( ).OnMenu( menuName, selection, primitiveSelection );
        Destroy( GetComponent< Button >( ));
        border.color = disabledColor;
    }

    void Start( ) {

        canvasGroup.alpha = 0;
        startTime = Time.time;
    }

    Vector3 GetWorldCenter( Transform t ) {

        var corners = new Vector3[ 4 ];
        t.GetComponent< RectTransform >( ).GetWorldCorners( corners );
        var center = ( corners[ 0 ] + corners[ 2 ]) / 2;

        return center;
    }

    void Update( ) {

        counter ++;
    
        //initialize on third frame, because the grid may not have set the positions yet
        if( counter == counterThreshold ) {

            var center = GetWorldCenter( transform );
            var localCenter = transform.parent.InverseTransformPoint( center );
            localTargetCenter = localCenter;
        }

        if( counter > counterThreshold ) {

            var zC = Mathf.Clamp( z, -1, 1 );
            //canvasGroup.alpha = 1 - Mathf.Abs( zC );
            canvasGroup.alpha = 1;
            transform.localScale = Vector3.one * ( 1 - zC );

            var screenCenter = new Vector3( Screen.width, Screen.height ) / 2;
            var localScreenCenter = transform.parent.InverseTransformPoint( screenCenter );
            var linear = localTargetCenter - localScreenCenter;
            var offset = localScreenCenter;
            transform.localPosition = linear * ( 1 - zC ) + offset;
        }

        PlayOneFrame( Time.deltaTime );
    }

    public void SetImmediateZ( float zIn ) {

        SetImmediate( Vector3.forward * zIn );
    }

    public void SetTargetZ( float zIn ) {

        SetTarget( Vector3.forward * zIn, true );
        _interpolate -= Random.value * 0.5f; //small time delay
    }

    protected override Vector3 GetAnimatedProperty( ) {

        return Vector3.forward * z;
    }

    protected override void SetAnimatedProperty( Vector3 target ) {

        z = target.z;
    }

    public void OnPointerEnter( PointerEventData e ) {

        if( _interpolate < 1 ) return; //do not play sound during animation
        Master.Get( ).OnMenuButtonPointerEnter( );
    }
}


}