using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SmartEditor;
using System.Linq;
using System;

namespace Amand {


public class SoftwareLayerSwitch : MonoBehaviour {
    
    public string[ ] layerMask = new string[ ]{ };
    
    public HashSet< string > GetMaskedLayers( ) {

        var allLayers = GetAllLayers( );
        var filled = false;
        var layers = new HashSet< string >( );
        
        Action Fill = ( ) => { 

            foreach( var layer in allLayers ) layers.Add( layer ); 
            filled = true;
        };

        foreach( var item in layerMask ) {

            var expression = item.Trim( );
            var negate = false;

            if( expression == "" ) {

                //ignore
            }
            else if( expression.Substring( 0, 1 ) == "-" ) {

                expression = expression.Substring( 1 ).Trim( );
                negate = true;
            }
            else if( expression.Substring( 0, 1 ) == "+" ) {

                expression = expression.Substring( 1 ).Trim( );
            }
            
            if( expression == "everything" || expression == "all" || expression == "any" || expression == "*" ) {

                if( ! negate ) Fill( );
            }
            else if( expression == "nothing" || expression == "none" || expression == "/" ) {

                if( negate ) Fill( );
            }
            else {

                if( negate ) {

                    if( ! filled ) Fill( );
                    layers.Remove( expression );
                }
                else {

                    layers.Add( expression );
                }
            }
        }

        return layers;
    }

    public static HashSet< string > GetAllLayers( ) {

        var allLayers = new HashSet< string >( );
        foreach( var item in FindObjectsOfType< SoftwareLayer >( true )) {

            allLayers.Add( item.layer );
        }

        return allLayers;
    }

    public void Mask( ) {

        MaskInternal( GetMaskedLayers( ));
    }

    static void MaskInternal( HashSet< string > masked ) {

        foreach( var sl in FindObjectsOfType< SoftwareLayer >( true )) {

            if( ! sl.ignore ) sl.gameObject.SetActive( masked.Contains( sl.layer ));   
        }
    }

    public static void MaskEverything( ) {

        MaskInternal( GetAllLayers( ));
    }

    public static void MaskNothing( ) {

        MaskInternal( new HashSet< string >( ));
    }
}

#if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( SoftwareLayerSwitch ))]
    public class SoftwareLayerSwitchInspector : SmartEditor< SoftwareLayerSwitch > {

        protected override void OnSmartInspectorGUI( SoftwareLayerSwitch target ) {
            
            Expose( "layerMask" );

            var allLayers = SoftwareLayerSwitch.GetAllLayers( );
            var layers = target.GetMaskedLayers( );
            var note = "";
            foreach( var layer in allLayers ) {

                var tick = layers.Contains( layer ) ? "√" : " ";
                note += $"[{ tick }] { layer }\n";
            }

            Note( note.Trim( ));
        }
    }
    
#endif


}
