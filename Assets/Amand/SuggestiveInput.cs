using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Amand {


public class SuggestiveInput : MonoBehaviour {
    
    public InputField input;
    public Transform suggestionsParent;
    public Suggestion suggestionPrefab;
    public ProtocolInputButton inputButton;

    public string lastHash = "";

    void Start( ) {

        Rebuild( );
    }

    void Rebuild( ) {

        var options = Master.Get( ).GetKnownValues( inputButton.input.identifier );
        var remaining = options.Where( option => option.ToLower( ).StartsWith( input.text.ToLower( )) && option.ToLower( ).Length > input.text.ToLower( ).Length ).Take( 3 );
        Master.ClearChildren( suggestionsParent );

        var active = inputButton.input.suggestInputs;
        suggestionsParent.gameObject.SetActive( active );

        if( active ) {

            foreach( var option in remaining ) {

                var suggestion = Instantiate( suggestionPrefab, suggestionsParent );
                suggestion.label.text = option;
                suggestion.parent = this;
            }
        }
    }

    public void OnClick( Suggestion source ) {

        input.text = source.label.text;
        Rebuild( );
    }

    void Update( ) {

        var hash = Master.Hash( input.text );

        if( lastHash != hash ) {

            Rebuild( );
            lastHash = hash;
        }
    }
}


}