using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Amand {

public class Cursor : MonoBehaviour {

    private static Cursor instance;
    public static Cursor Get( ) { return instance; }
    void Awake( ) { instance = this; SetNormal( ); }

    public Texture2D normal;
    public Texture2D pointer;
    public Vector2 offset = new Vector2( 10, 8 );

    public void SetNormal( ) {

        UnityEngine.Cursor.SetCursor( normal, offset, CursorMode.Auto );
    }

    public void SetPointer( ) {

        UnityEngine.Cursor.SetCursor( pointer, offset, CursorMode.Auto );
    }
}


}

//https://mac-cursors.netlify.app