using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsPosition : MonoBehaviour {
    
    Vector2 _positionInBounds;

    public Vector2 positionInBounds {

        get => _positionInBounds;
        set { 
        
            _positionInBounds = value; 
            var rt = GetComponent< RectTransform >( );
            rt.pivot = rt.anchorMin = rt.anchorMax = _positionInBounds;
            rt.anchoredPosition = Vector2.zero;    
        }
    }
}
