using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class TableData : MonoBehaviour {

    public RectOffset padding;
    public Text textPrefab;
    public TableInput inputPrefab;
    public Transform togglePrefab;
    public Transform imagePrefab;
    public Image image;
    public Color normal;
    public Color editing;

    public bool isReadonly;
    public Text text;
    public TableInput input;
    public TableRow parent;
    public Toggle toggle;
    public Image imageContent;
    public bool isEditing;

    public void SetColumnWidth( float totalWidth ) {

        var remainingWidth = totalWidth - padding.left - padding.right;
        if( Content( ) != null ) Content( ).SetSizeWithCurrentAnchors( RectTransform.Axis.Horizontal, remainingWidth );
        ( transform as RectTransform ).SetSizeWithCurrentAnchors( RectTransform.Axis.Horizontal, totalWidth );
    }

    RectTransform Content( ) {

	if( text != null ) return text.transform as RectTransform;
	if( input != null ) return input.transform as RectTransform;
	if( toggle != null ) return toggle.transform.parent as RectTransform;
        if( imageContent != null ) return imageContent.transform.parent as RectTransform;
	return null;
    }

    public string GetData( ) {

        if( text != null ) return text.text;
        if( input != null ) return input.inputField.text;
        if( toggle != null ) return toggle.isOn ? "true" : "false";
        if( imageContent != null ) return "[image]";
        return null;
    }

    public void Clear( ) {

        while( transform.childCount > 0 ) {

            var child = transform.GetChild( 0 );
            child.SetParent( null );
            Destroy( child.gameObject );
        }

        text = null;
        input = null;
        toggle = null;
        imageContent = null;
    }

    public float CalculateHeight( ) {

	float height = 10;
	if( Content( ) != null ) height = Content( ).rect.height;
	height += padding.top + padding.bottom;
	return height;
    }

    public void UpdateHeight( ) {

	var h = CalculateHeight( );
        ( transform as RectTransform ).SetSizeWithCurrentAnchors( RectTransform.Axis.Vertical, h );
    }

    public Text AddText( ) {

	var child = Instantiate( textPrefab );
        child.transform.SetParent( transform );
        text = child;
        return child;
    }

    public TableInput AddTableInput( ) {

	var child = Instantiate( inputPrefab );
        child.transform.SetParent( transform );
        child.parent = this;
        input = child;
        return child;
    }

    public Toggle AddToggle( ) {

	var child = Instantiate( togglePrefab );
        child.transform.SetParent( transform );
        toggle = child.GetChild( 0 ).GetComponent< Toggle >( );
        return toggle;
    }

    public Image AddImage( ) {

        var child = Instantiate( imagePrefab );
        child.transform.SetParent( transform );
        imageContent = child.GetChild( 0 ).GetComponent< Image >( );
        return imageContent;
    }

    public void OnClick( ) {

	parent.OnClick( );
    }

    public void OnBeginEdit( ) {

	parent.OnClick( );
	isEditing = true;
    }

    public void OnEndEdit( ) {

	parent.OnClick( );
	isEditing = false;
    }

    public void OnPointerEnter( ) {

	parent.OnPointerEnter( );
    }

    public void OnPointerExit( ) {

	parent.OnPointerExit( );
    }

    void Update( ) {

	image.color = normal;
        if( ! isReadonly && isEditing ) image.color = editing;
    }
}


}