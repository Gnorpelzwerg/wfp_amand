using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Amand {


public class AutoDetectInput : MonoBehaviour {
    
    public Well.Input input;

    void Awake( ) {

        var mobilePlatforms = new RuntimePlatform[ ]{ RuntimePlatform.IPhonePlayer, RuntimePlatform.Android };
        input._touchControls = System.Array.IndexOf( mobilePlatforms, Application.platform ) > -1;
    }
}


}