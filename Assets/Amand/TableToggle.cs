using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class TableToggle : MonoBehaviour {

    public Image background;
    public Color colorOff;
    public Color colorOn;
    public Toggle uiToggle;

    public bool isOn( ) {

        return uiToggle.isOn;
    }

    void Update( ) {

        background.color = isOn( ) ? colorOn : colorOff;
    }
}


}
