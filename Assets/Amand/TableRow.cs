using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class TableRow : MonoBehaviour {

    public TableData dataPrefab;
    public List< TableData > dataElements;
    public Image image;
    public Color normal;
    public Color hover;
    public Color selected;

    public bool isReadonly;
    public bool isHovered;
    public bool isSelected;

    public void SetColumnWidths( float totalWidth, float[ ] widths ) {

        var layout = GetComponent< HorizontalLayoutGroup >( );
        var count = dataElements.Count;
        var remainingWidth = totalWidth - layout.padding.left - layout.padding.right - Mathf.Max( 0, count - 1 ) * layout.spacing;

        for( int i = 0; i < count; ++ i ) {

            var data = dataElements[ i ];
            data.SetColumnWidth( widths[ i ] * remainingWidth );
        }

        ( transform as RectTransform ).SetSizeWithCurrentAnchors( RectTransform.Axis.Horizontal, totalWidth );
    }

    public void Clear( ) {

        while( transform.childCount > 0 ) {

            var child = transform.GetChild( 0 );
            child.SetParent( null );
            Destroy( child.gameObject );
        }

        foreach( var data in dataElements ) data.Clear( );
        dataElements.Clear( );
    }

    public TableData AddData( ) {

	var child = Instantiate( dataPrefab );
	child.parent = this;
        child.transform.SetParent( transform );
        dataElements.Add( child );
        return child;
    }

    public float CalculateHeight( ) {

	var layout = GetComponent< HorizontalLayoutGroup >( );
        float max = 0;
        foreach( var data in dataElements ) max = Mathf.Max( max, data.CalculateHeight( ));
        max += layout.padding.top + layout.padding.bottom + Mathf.Max( 0, dataElements.Count - 1 ) * layout.spacing;
        return max;
    }

    public void UpdateHeight( ) {

        foreach( var data in dataElements ) data.UpdateHeight( );
        ( transform as RectTransform ).SetSizeWithCurrentAnchors( RectTransform.Axis.Vertical, CalculateHeight( ));
    }

    void Update( ) {

	image.color = normal;

	if( ! isReadonly ) {

		if( isHovered ) image.color = hover;
		if( isSelected ) image.color = selected;
	}
    }

    public void OnPointerEnter( ) {

	isHovered = true;
    }

    public void OnPointerExit( ) {

	isHovered = false;
    }

    public void OnClick( ) {

	Master.Get( ).activeScreenContent.SetSelectedRow( this );
    }
}


}
