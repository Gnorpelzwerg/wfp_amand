using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Amand {

public class Logo : MonoBehaviour {

    public GameObject tooltip;
    public bool disabled;

    void Start( ) {

        Unhover( );
    }

    void Update( ) {

        if( ! gameObject.activeInHierarchy ) Unhover( );
    }

    void Unhover( ) {

        tooltip.SetActive( false );
        Cursor.Get( ).SetNormal( );
    }

    void Hover( ) {

        tooltip.SetActive( true );
        Cursor.Get( ).SetPointer( );
    }

    public void OnClick( ) {

        if( disabled ) return;
        Master.Get( ).OnHome( );
        Unhover( );
    }

    public void OnEnter( ) {

        if( disabled ) return;
        Hover( );
    }

    public void OnExit( ) {

        Unhover( );
    }
}


}