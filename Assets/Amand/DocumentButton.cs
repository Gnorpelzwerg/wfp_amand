using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Amand.Structure;

namespace Amand {


public class DocumentButton : MonoBehaviour {

    public Structure.Document document;
    public Image icon;
    public Image background;
    public Sprite textIcon;
    public Sprite imageIcon;
    public Sprite audioIcon;
    public Sprite sceneIcon;
    public Sprite videoIcon;
    public Sprite contentsIcon;

    public void OnClick( ) {

    	Master.Get( ).OnDocument( document );
    }

    public void MarkActive( bool active ) {

    	background.gameObject.SetActive( active );
    }

    void Update( ) {

    	if( document != null ) {

            icon.sprite = GetSprite( );
    	}
    }

    Sprite GetSprite( ) {

        switch( document.type ) {

            case DocumentType.text: return textIcon;
            case DocumentType.image: return imageIcon;
            case DocumentType.audio: return audioIcon;
            case DocumentType.scene: return sceneIcon;
            case DocumentType.video: return videoIcon;
            case DocumentType.tableOfContents: return contentsIcon;
            default: return textIcon;
        }
    }
}


}